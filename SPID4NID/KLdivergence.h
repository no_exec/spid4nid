/*
 * KLdivergence.h
 *
 *  Created on: 7.4.2014
 *      Author: adam
 */

#ifndef KLDIVERGENCE_H_
#define KLDIVERGENCE_H_

#include "Metric.h"

class KLdivergence : public Metric {
public:
	KLdivergence();
	virtual ~KLdivergence();

	float computeSimilarity(attr_fingerprint protocol,
				attr_fingerprint protocol_model);
};

#endif /* KLDIVERGENCE_H_ */
