/*
 * ExhaustiveOptimizer.h
 *
 *  Created on: 29.9.2014
 *      Author: adam
 */

#ifndef EXHAUSTIVEOPTIMIZER_H_
#define EXHAUSTIVEOPTIMIZER_H_

#include "Optimizer.h"

class ExhaustiveOptimizer : public Optimizer {
public:
	static test_dir_results results;

	ExhaustiveOptimizer();
	virtual ~ExhaustiveOptimizer();

	uint optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, std::ostream& log = std::cout, uint mode = 0);
	static float Objective(bool* config, uint len);
};

#endif /* EXHAUSTIVEOPTIMIZER_H_ */
