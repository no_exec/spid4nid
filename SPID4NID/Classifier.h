/*
 * Classifier.h
 *
 *  Created on: 18.3.2014
 *      Author: adam
 */

#ifndef CLASSIFIER_H_
#define CLASSIFIER_H_

#include <tuple>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include "definitions.h"
#include "Features/Feature.h"
#include "Flow.h"
#include "Metric.h"

#define SUCCESS 0
#define FINISH 1
#define TRAIN_MODE 2
#define FINISH_TRAIN 3

class Classifier {
private:
	std::vector<Feature*> m_features;
	std::vector<Metric*> m_metrics;
	proto_fprints m_protocol_fingerprints;
	configuration m_config;
	uint* m_readiness;
public:
	Classifier();
	virtual ~Classifier();

	configuration getConfiguration();
	void setConfiguration(configuration config);
	proto_fprints* getProtocolFingerprints();
	void setProtocolFingerprints(proto_fprints fprints);
	uint train(Flow* p_flow, std::string protocol, std::ostream& log = std::cout, uint mode = 0);
	uint classify(Flow* p_flow, classif_result* results, std::ostream& log = std::cout,
			uint mode = 0);
	uint numFeatures();
	uint numProtocols();
};

#endif /* CLASSIFIER_H_ */
