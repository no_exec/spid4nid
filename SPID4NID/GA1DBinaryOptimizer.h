/*
 * GA1DBinaryOptimizer.h
 *
 *  Created on: 28.9.2014
 *      Author: adam
 */

#ifndef GA1DBINARYOPTIMIZER_H_
#define GA1DBINARYOPTIMIZER_H_

#include <ga/GA1DBinStrGenome.h>
#include <ga/GASimpleGA.h>
#include <ga/GASStateGA.h>
#include <iostream>
#include "Optimizer.h"

class GA1DBinaryOptimizer : public Optimizer {
public:
	static test_dir_results results;

	GA1DBinaryOptimizer();
	virtual ~GA1DBinaryOptimizer();

	uint optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, std::ostream& log = std::cout, uint mode = 0);
	static float Objective(GAGenome &);
};

#endif /* GA1DBINARYOPTIMIZER_H_ */
