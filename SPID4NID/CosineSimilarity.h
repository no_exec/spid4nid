/*
 * CosineSimilarity.h
 *
 *  Created on: 7.4.2014
 *      Author: adam
 */

#ifndef COSINESIMILARITY_H_
#define COSINESIMILARITY_H_

#include "Metric.h"

class CosineSimilarity : public Metric {
public:
	CosineSimilarity();
	virtual ~CosineSimilarity();

	float computeSimilarity(attr_fingerprint protocol,
				attr_fingerprint protocol_model);
};

#endif /* COSINESIMILARITY_H_ */
