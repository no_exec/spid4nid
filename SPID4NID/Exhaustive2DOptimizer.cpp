/*
 * Exhaustive2DOptimizer.cpp
 *
 *  Created on: 1.10.2014
 *      Author: adam
 */

#include <iostream>
#include <tuple>
#include "Exhaustive2DOptimizer.h"

using namespace std;

test_dir_results Exhaustive2DOptimizer::results;

Exhaustive2DOptimizer::Exhaustive2DOptimizer() {
	// TODO Auto-generated constructor stub

}

Exhaustive2DOptimizer::~Exhaustive2DOptimizer() {
	// TODO Auto-generated destructor stub
}

uint Exhaustive2DOptimizer::optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, ostream& log, uint mode) {
	results = overall_results;
	bool config[width];
	for (uint i = 0; i < width; i++) config[i] = false;
	float best_scores[height];
	for (uint i = 0; i < height; i++) best_scores[i] = 0;
	bool** best_configs;
	bool found;
	best_configs = new bool *[height];
	for (uint i = 0; i < height; i++) best_configs[i] = new bool[width];
	for (uint i = 0; i < height; i++) {
		do {
			found = false;
			for (uint j = 0; j < width; j++) {
				if (!config[j]) {
					found = true;
					config[j] = true;
					for (uint k = 0; k < j; k++) {
						config[k] = false;
					}
					break;
				}
			}
			if (!found) break;
			float score = Objective(config, width, height, i);
			if (score > best_scores[i]) {
				best_scores[i] = score;
				for (uint j = 0; j < width; j++) best_configs[i][j] = config[j];
			}
		} while (true);
	}
	float score = OverAllObjective(best_configs, width, height);
	log << "Best score: " << score << endl;
	solution->dim2 = (bool**)malloc(sizeof(bool*)*height);
	for (uint i = 0; i < height; i++) {
		solution->dim1[i] = (bool*)malloc(sizeof(bool)*width);
		for (uint j = 0; j < width; j++) {
			if (best_configs[i][j]) log << "1";
			else log << "0";
			solution->dim2[i][j] = best_configs[i][j];
		}
		log << endl;
	}
	solution->height = height;
	solution->width = width;
	for (uint i = 0; i < height; i++) {
		delete best_configs[i];
	}
	delete best_configs;
	return 0;
}

float Exhaustive2DOptimizer::Objective(bool* config, uint width, uint height, uint num) {
	float score = 0;
		for (uint i = 0; i < results.size(); i++) {
			string file_proto_name = results[i].first;
			for (uint j = 0; j < results[i].second.size(); j++) {
				for (uint k = 0; k < results[i].second[j].size(); k++) {
					if (k/width != num) continue;
					string proto_name = std::get<0>(results[i].second[j].at(k));
					float sim = std::get<2>(results[i].second[j].at(k));
					uint x = k % width;
					if (config[x]) {
						if (file_proto_name.compare(0,file_proto_name.length(),proto_name,0,file_proto_name.length())) {
							if (sim > 0.8) score -= sim;
							else score += 1 - sim;
						} else {
							if (sim > 0.8) score += sim;
							else score -= 1 - sim;
						}
					}
				}
			}
		}
		return (score > 0) ? score : 0;
}

float Exhaustive2DOptimizer::OverAllObjective(bool** config, uint width, uint height) {
	float score = 0;
		for (uint i = 0; i < results.size(); i++) {
			string file_proto_name = results[i].first;
			for (uint j = 0; j < results[i].second.size(); j++) {
				for (uint k = 0; k < results[i].second[j].size(); k++) {
					string proto_name = std::get<0>(results[i].second[j].at(k));
					float sim = std::get<2>(results[i].second[j].at(k));
					uint x = k / width;
					uint y = k % width;
					if (config[x][y]) {
						if (file_proto_name.compare(0,file_proto_name.length(),proto_name,0,file_proto_name.length())) {
							if (sim > 0.8) score -= sim;
							else score += 1 - sim;
						} else {
							if (sim > 0.8) score += sim;
							else score -= 1 - sim;
						}
					}
				}
			}
		}
		return (score > 0) ? score : 0;
}
