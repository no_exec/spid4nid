/*
 * ExhaustiveOptimizer.cpp
 *
 *  Created on: 29.9.2014
 *      Author: adam
 */

#include <iostream>
#include <tuple>
#include <string>
#include "ExhaustiveOptimizer.h"

using namespace std;

test_dir_results ExhaustiveOptimizer::results;

ExhaustiveOptimizer::ExhaustiveOptimizer() {
	// TODO Auto-generated constructor stub

}

ExhaustiveOptimizer::~ExhaustiveOptimizer() {
	// TODO Auto-generated destructor stub
}

uint ExhaustiveOptimizer::optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, ostream& log, uint mode) {
	results = overall_results;
	bool config[width];
	for (uint i = 0; i < width; i++) config[i] = false;
	float best_scores[5];
	for (uint i = 0; i < 5; i++) best_scores[i] = 0;
	bool best_configs[5][width],
		 found = true;
	float max = best_scores[0];
	uint max_pos = 0;
	do {
		found = false;
		for (uint i = 0; i < width; i++) {
			if (!config[i]) {
				found = true;
				config[i] = true;
				for (uint j = 0; j < i; j++) {
					config[j] = false;
				}
				break;
			}
		}
		if (!found) break;

		float score = Objective(config, width);
		float min = best_scores[0];
		uint min_pos = 0;

		for (uint i = 1; i < 5; i++) {
			if (min > best_scores[i]) {
				min = best_scores[i];
				min_pos = i;
			}
		}
		if (score > min) {
			best_scores[min_pos] = score;
			for (uint i = 0; i < width; i++) best_configs[min_pos][i] = config[i];
			if (score > max) {
				max = score;
				max_pos = min_pos;
			}
		}
	} while (true);
	log << "Top 5 scores: " << endl;
	for (uint i = 0; i < 5; i++) {
		log << best_scores[i] << ": ";
		for (uint j = 0; j < width; j++) {
			if (best_configs[i][j])
				log << "1";
			else
				log << "0";
		}
		log << endl;
		solution->dim1 = (bool*)malloc(sizeof(bool)*width);
		for (uint i = 0; i < width; i++) solution->dim1[i] = best_configs[max_pos][i];
	}
	return 0;
}

float ExhaustiveOptimizer::Objective(bool* config, uint len) {
	float score = 0;
		for (uint i = 0; i < results.size(); i++) {
			string file_proto_name = results[i].first;
			for (uint j = 0; j < results[i].second.size(); j++) {
				for (uint k = 0; k < results[i].second[j].size(); k++) {
					string proto_name = std::get<0>(results[i].second[j].at(k));
					float sim = std::get<2>(results[i].second[j].at(k));
					uint x = k % len;
					if (config[x]) {
						if (file_proto_name.compare(0,file_proto_name.length(),proto_name,0,file_proto_name.length())) {
							if (sim > 0.8) score -= sim;
							else score += 1 - sim;

						} else {
							
							if (sim > 0.8) score += sim;
							else score -= 1 - sim;
						}
					}
				}
			}
		}
		return (score > 0) ? score : 0;
}
