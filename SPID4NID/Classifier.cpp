/*
 * Classifier.cpp
 *
 *  Created on: 18.3.2014
 *      Author: adam
 */

//#include <tuple>
#include "Classifier.h"
#include "Features/ByteFrequencyFeature.h"
#include "Features/AccumulatedDirectionBytesFeature.h"
#include "Features/DirectionByteCountFeature.h"
#include "Features/DirectionByteFrequencyFeature.h"
#include "Features/NewLineEqualityFeature.h"
#include "Features/NullFrequencyFeature.h"
#include "Features/DirectionEntropyFeature.h"
#include "Features/BytePairsReoccurring32.h"
#include "Features/DirectionChangesFeature.h"
#include "Features/First32BytesFrequency.h"
#include "Features/First4BytesCrossSum.h"
#include "Features/FirstNonEmptyPayloadSize.h"
#include "Features/ActionReactionFirst3ByteHashFeature.h"
#include "Features/Base64EncodingFeature.h"
#include "Features/ByteVariance.h"
#include "Features/ControlCharacterFrequency.h"
#include "Features/ControlCharacterRatio.h"
#include "Features/EntropyFeature.h"
#include "Features/PacketLengthPairsReoccurring.h"
#include "Features/PayloadSizeFrequency.h"
#include "Features/PayloadSizeHashPairs.h"
#include "Features/ThreeByteHashFeature.h"
#include "Features/TwoByteHashFeature.h"
#include "Features/FirstBitPositionsFeature.h"
#include "Features/PayloadSizeChangesFeature.h"
#include "Features/PayloadSizesFeature.h"
#include "Features/ByteBitValueFeature.h"
#include "Features/FirstPacketPerDirectionNibblesFeature.h"
#include "Features/FourByteHashFeature.h"
#include "Features/NibblePositionFrequencyFeature.h"
#include "KLdivergence.h"
#include "CosineSimilarity.h"
#include <cstring>
#include <iostream>
#include <fstream>
#include <cmath>

using std::string;
using std::vector;
using std::map;
using std::ostream;
using std::isnan;

Classifier::Classifier() {
	m_features = std::vector<Feature*>(30);
	m_features = {
		new ByteFrequencyFeature(0),
		new AccumulatedDirectionBytesFeature(1),
		new DirectionByteCountFeature(2),
		new DirectionByteFrequencyFeature(3),
		new NewLineEqualityFeature(4),
		new NullFrequencyFeature(5),
		new DirectionEntropyFeature(6),
		new BytePairsReoccurring32(7),
		new DirectionChangesFeature(8),
		new First32BytesFrequency(9),
		new First4BytesCrossSum(10),
		new FirstNonEmptyPayloadSize(11),
		new ActionReactionFirst3ByteHashFeature(12),
		new Base64EncodingFeature(13),
		new ControlCharacterFrequency(14),
		new ControlCharacterRatio(15),
		new PayloadSizeFrequency(16),
		new PayloadSizeHashPairs(17),
		new ThreeByteHashFeature(18),
		new TwoByteHashFeature(19),
		new FirstBitPositionsFeature(20),
		new PayloadSizeChangesFeature(21),
		new ByteBitValueFeature(22),
		new FirstPacketPerDirectionNibblesFeature(23),
		new FourByteHashFeature(24),
		new PayloadSizesFeature(25),
		new NibblePositionFrequencyFeature(26),
		new EntropyFeature(27),
		new PacketLengthPairsReoccurring(28),
		new ByteVariance(29)
	};
	m_readiness = new uint[30];
	memset(m_readiness, 2, 30 * sizeof(uint));
	m_metrics = std::vector<Metric*>(2);
	m_metrics[0] = new KLdivergence();
	m_metrics[1] = new CosineSimilarity();
}

Classifier::~Classifier() {
	m_features.clear();
	delete[] m_readiness;
	m_metrics.clear();
	m_protocol_fingerprints.clear();
	configuration conf; //empty configuration
	this->setConfiguration(conf);
}

uint Classifier::train(Flow* p_flow, string protocol, ostream& log, uint mode) {
	if (mode == FINISH) {
		if (!m_protocol_fingerprints.empty()) {

			classif_result results;
			this->classify(p_flow, &results, log, FINISH_TRAIN);
			classif_result::iterator iter;
			log
					<< "Training: " + protocol
							+ std::to_string(p_flow->m_id) << std::endl;
			float avg = 0, max = 0;
			string proto;
			uint c = 0;
			for (iter = results.begin(); iter != results.end(); iter++) {
				float temp = fabs(std::get<2>(*iter));
				avg += temp;
				c++;
				if (c == m_features.size()) { // should be the number of implemented features
					avg /= (float) m_features.size();

					if (avg > max) {
						max = avg;
						proto.assign(std::get<0>(*iter));

					}

					avg = 0;
					c = 0;
				}
			}

			log << "Most similar protocol: " << proto << std::endl;
			if (max > 0.7) {
				std::map<uint, attr_fingerprint> fprints =
						m_protocol_fingerprints[proto];
				std::map<uint, attr_fingerprint>::iterator iter2;
				std::map<uint, attr_fingerprint> new_map_fpr;
				for (iter2 = fprints.begin(); iter2 != fprints.end(); iter2++) {
					attr_fingerprint at_fpr1 = iter2->second;
					attr_fingerprint at_fpr2 =
							p_flow->fingerprints[iter2->first];
					attr_fingerprint new_fprint;
					if (m_features[iter2->first]->m_merge) {
						new_fprint = m_features[iter2->first]->mergeFeatureVectors(at_fpr1, at_fpr2);
					} else {
						new_fprint.counter_vec = std::vector<uint>(
								at_fpr1.counter_vec.size(), 0);
						new_fprint.probability_vec = std::vector<float>(
								at_fpr1.probability_vec.size(), 0.0f);
						uint sum = 0;
						for (uint i = 0; i < at_fpr1.counter_vec.size(); i++) {
							new_fprint.counter_vec[i] = at_fpr1.counter_vec[i]
									+ at_fpr2.counter_vec[i];
							sum += new_fprint.counter_vec[i];
						}
						for (uint i = 0; i < at_fpr1.probability_vec.size(); i++) {
							new_fprint.probability_vec[i] =
									(float) new_fprint.counter_vec[i] / (float) sum;
						}
					}
					new_map_fpr[iter2->first] = new_fprint;
				}
				m_protocol_fingerprints[proto] = new_map_fpr;
				log << "Merged with " + proto << std::endl;
				return SUCCESS;
			}
		}
		m_protocol_fingerprints[protocol + std::to_string(p_flow->m_id)] =
				p_flow->fingerprints;
		return SUCCESS;
	}
	return this->classify(p_flow, NULL, log, TRAIN_MODE);

}

uint Classifier::classify(Flow* p_flow, classif_result* results, ostream& log,
		uint mode) {
	if (mode == FINISH) {
		classif_result res(m_protocol_fingerprints.size());
		map<string, map<uint, attr_fingerprint> >::iterator iter;
		map<uint, attr_fingerprint>::iterator iter2;
		int i = 0, j = 0, k = 0, c = 0;
		float sim = 0;
		bool* conf = NULL;
		for (iter = m_protocol_fingerprints.begin();
				iter != m_protocol_fingerprints.end(); iter++) {
			if (m_config.dim1)
				conf = m_config.dim1;
			if (m_config.dim2)
				conf = m_config.dim2[j++];
			k = 0;
			
			for (iter2 = p_flow->fingerprints.begin();
					iter2 != p_flow->fingerprints.end(); iter2++) {
				
				if (conf[k++]) {
					
					sim += fabs(
							m_metrics[1]->computeSimilarity(iter2->second,
									iter->second[iter2->first]));
					c++;

				}
			}
			float similarity = sim / (float)c;
			std::get<0>(res[i]) = iter->first;
			std::get<1>(res[i]) = 0;
			std::get<2>(res[i]) = (isnan(similarity) ? 0 : similarity);

			i++;
			sim = 0;
			c = 0;
		}
		*results = res;
		return SUCCESS;
	}
	if (mode == FINISH_TRAIN) {
		classif_result res(m_protocol_fingerprints.size() * m_features.size());
		proto_fprints::iterator iter;
		map<uint, attr_fingerprint>::iterator iter2;
		int i = 0;
		float sim = 0;
		for (iter = m_protocol_fingerprints.begin();
				iter != m_protocol_fingerprints.end(); iter++) {
			for (iter2 = p_flow->fingerprints.begin();
					iter2 != p_flow->fingerprints.end(); iter2++) {
				
				std::get<0>(res[i]) = iter->first;
				
				sim = m_metrics[1]->computeSimilarity(iter2->second,
						iter->second[iter2->first]);
				
				std::get<1>(res[i]) = iter2->first;
				std::get<2>(res[i]) = (isnan(sim) ? 0 : sim);

				i++;
			}
		}
		*results = res;
		return SUCCESS;
	}
	if (mode == TRAIN_MODE) {
		vector<Feature*>::iterator iter;
		for (iter = m_features.begin(); iter != m_features.end(); iter++) {
			m_readiness[(*iter)->m_id] = (*iter)->updateFeature(p_flow);
		}
	}
	if (mode == 0) {
		vector<Feature*>::iterator iter;
		uint i = 0;
		for (iter = m_features.begin(); iter != m_features.end(); iter++) {
			if (m_config.dim1) {
				if (m_config.dim1[i++])
					m_readiness[(*iter)->m_id] = (*iter)->updateFeature(p_flow);
			} else {
				m_readiness[(*iter)->m_id] = (*iter)->updateFeature(p_flow);
			}
		}
	}
	if (p_flow->packet_buffer.size() > 10) {
		for (uint i = 0; i < p_flow->packet_buffer.size(); i++) {
			if (!p_flow->packet_buffer.at(i).keep_stored) {
				delete p_flow->packet_buffer.at(i).data;
				p_flow->packet_buffer.erase(p_flow->packet_buffer.begin() + i);
			}
		}
	}
	bool ready = true;
	for (uint i = 0; i < m_features.size(); i++) {
		if (m_readiness[i] != SUCCESS) {
			ready = false;
			break;
		}
	}
	if (ready) {
		return FINISH;
	}
	return SUCCESS;
}

uint Classifier::numFeatures() {
	return m_features.size();
}

uint Classifier::numProtocols() {
	return m_protocol_fingerprints.size();
}

configuration Classifier::getConfiguration() {
	return m_config;
}

void Classifier::setConfiguration(configuration config) {
	if (m_config.dim1)
		delete[] m_config.dim1;
	if (m_config.dim2) {
		for (uint i = 0; i < m_config.height; i++)
			delete[] m_config.dim2[i];
		delete[] m_config.dim2;
	}
	m_config = config;
}

proto_fprints* Classifier::getProtocolFingerprints() {
	return &m_protocol_fingerprints;
}

void Classifier::setProtocolFingerprints(proto_fprints fprints) {
	m_protocol_fingerprints.clear();
	m_protocol_fingerprints = fprints;
}
