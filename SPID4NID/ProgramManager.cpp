/*
 * ProgramManager.cpp
 *
 *  Created on: 3.2.2015
 *      Author: adam
 */

#include "ProgramManager.h"
#include "tinydir.h"
#include <cmath>
#include <cstring>
#include <cstdlib>

using std::cout;
using std::cin;
using std::string;
using std::isnan;

ProgramManager::ProgramManager() {
	// TODO Auto-generated constructor stub
	pmng = new ProtocolManager();
	xml = new XMLSerializer();
}

ProgramManager::~ProgramManager() {
	// TODO Auto-generated destructor stub
	delete pmng;
	delete xml;
}

int ProgramManager::storeState(string output_file, uint mode) {
	xml->storeState(pmng->getClassifier(), output_file.c_str(), mode);
	return 0;
}

int ProgramManager::loadState(string input_file) {
	xml->loadState(pmng->getClassifier(), input_file.c_str());
	return 0;
}

int ProgramManager::train(string train_dir, string val_dir, string output_log,
		std::string output_state, uint state_mode) {
	std::ostream* output = &std::cout;
	if (!output_log.empty()) {
		output = new std::ofstream(output_log.c_str(), std::ofstream::out);
	}
	pmng->train(train_dir, val_dir, *output);
	if (!output_state.empty()) {
		this->storeState(output_state, state_mode);
	}
	*output << "Training is done!" << std::endl;
	return SUCCESS;
}

int ProgramManager::optimize(std::string val_dir, std::string output_state,
		std::string output_log, uint state_mode) {
	std::ostream* output = &std::cout;
	if (!output_log.empty()) {
		output = new std::ofstream(output_log.c_str(), std::ofstream::out);
	}
	pmng->validate(val_dir, *output);
	if (!output_state.empty()) {
		this->storeState(output_state, state_mode);
	}
	*output << "Optimization is done!" << std::endl;
	return SUCCESS;
}

int ProgramManager::classify(std::string class_dir, std::string output_log,
		uint mode) {
	std::ostream* output = &std::cout;
	if (!output_log.empty()) {
		output = new std::ofstream(output_log.c_str(), std::ofstream::out);
	}
	tinydir_dir dir;
	tinydir_open_sorted(&dir, class_dir.c_str());
	for (uint i = 0; i < dir.n_files; i++) {
		tinydir_file file;
		tinydir_readfile_n(&dir, &file, i);
		if (!strcmp(file.name,".") || !strcmp(file.name,"..")) continue;
		*output << "Classifying " << file.name << ":" << std::endl;
		test_results results;
		pmng->classify(class_dir + "/" + string(file.name), results, *output, 0);
		test_results::iterator iter;
		int c = 1;
		for (iter = results.begin(); iter != results.end(); iter++) {
			*output << "(" << file.name << ") " << "flow " << c++ << ":"
					<< std::endl;
			int num = iter->size() < 5 ? iter->size() : 5;
			if (num) {
				*output << "Protocol Probability" << std::endl;
				for (int i = 0; i < num; i++) {
					*output << std::get<0>(iter->at(i)) << " "
							<< std::get<2>(iter->at(i)) << std::endl;
				}
			}
			if (std::get<2>(iter->at(0)) > 0.8) {
				*output << "classified as " << std::get<0>(iter->at(0)) << std::endl;
			}
		}
	}
	*output << "Classification is done!" << std::endl;
	return SUCCESS;
}

int ProgramManager::test(std::string test_dir, std::string output_log,
		uint mode) {
	std::ostream* output = &std::cout;
	if (!output_log.empty()) {
		output = new std::ofstream(output_log.c_str(), std::ofstream::out);
	}
	tinydir_dir dir;
	tinydir_open_sorted(&dir, test_dir.c_str());
	int overallTP = 0, overallPrecN = 0, overallRecN = 0, overallAccN = 0, overallTN = 0;
	for (uint i = 0; i < dir.n_files; i++) {
		tinydir_file file;
		tinydir_readfile_n(&dir, &file, i);
		if (!strcmp(file.name,".") || !strcmp(file.name,"..")) continue;
		test_results results;
		pmng->classify(test_dir + "/" + file.name, results, *output, 0);
		std::string proto_name = std::string(file.name).substr(0,
				std::string(file.name).find(".pcap"));
		test_results::iterator iter;
		
		int tp = 0, c = 0, fp = 0, fn = 0, tn = 0;
		proto_fprints* protocols = pmng->getClassifier()->getProtocolFingerprints();
		*output << "Protocol " << proto_name << ":" << std::endl;
		for (iter = results.begin(); iter != results.end(); iter++) {
			if (!iter->empty()) {
				std::string first_proto = std::get<0>(iter->front());
				float prob = std::get<2>(iter->front());
				*output << "Flow" << c++ << " classified as: ";
				if (prob > 0.8) {
					if (proto_name.compare(0, proto_name.length(), first_proto, 0,
							proto_name.length()) == 0) {
						tp++;
					} else {
						fp++;
					}
					*output << first_proto << " with probability: " << prob << std::endl;
				} else {
					*output << "unknown" << std::endl;
					bool found = false;
					proto_fprints::iterator iter;
					for (iter = protocols->begin(); iter != protocols->end(); iter++) {
						if (proto_name.compare(0, proto_name.length(), iter->first, 0, proto_name.length()) == 0) {
							fn++;
							found = true;
							break;
						}
					}
					if (found == false) {
						*output << proto_name << " was not found in the database" << std::endl;
						tn++;
					}
				}
			}
		}
		float precision = (float) tp / (float) (tp + fp);
		precision = (isnan(precision) ? 0 : precision);
		*output << "Precision = " << precision << std::endl;
		float recall = (float) tp / (float) (tp + fn);
		recall = (isnan(recall) ? 0 : recall);
		*output << "Recall = " << recall << std::endl;
		float accuracy = (float) (tp + tn) / (float) (tp + fp + tn + fn);
		accuracy = (isnan(accuracy) ? 0 : accuracy);
		*output << "Accuracy = " << accuracy << std::endl;
		overallTP += tp;
		overallTN += tn;
		overallPrecN += tp + fp;
		overallRecN += tp + fn;
		overallAccN += tp + fp + tn + fn;
	}
	float overallPrec = (float) overallTP / (float) overallPrecN;
	overallPrec = (isnan(overallPrec) ? 0 : overallPrec);
	float overallRec = (float) overallTP / (float) overallRecN;
	overallRec = (isnan(overallRec) ? 0 : overallRec);
	float overallAcc = (float) (overallTP + overallTN) / (float) (overallAccN);
	overallAcc = (isnan(overallAcc) ? 0 : overallAcc);
	*output << "Overall precision = " << overallPrec << std::endl;
	*output << "Overall recall = " << overallRec << std::endl;
	*output << "Overall accuracy = " << overallAcc << std::endl;
	float fMeasure = 2 * overallPrec * overallRec / (overallPrec + overallRec);
	fMeasure = (isnan(fMeasure) ? 0 : fMeasure);
	*output << "F-measure = " << fMeasure << std::endl;
	*output << "Testing is done!" << std::endl;
	return SUCCESS;
}

