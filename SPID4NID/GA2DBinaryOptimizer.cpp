/*
 * GA1DBinaryOptimizer.cpp
 *
 *  Created on: 13.9.2014
 *      Author: adam
 */

#include <tuple>
#include <iostream>
#include <ga/GA1DBinStrGenome.h>
#include <ga/GA2DBinStrGenome.h>
#include "GA2DBinaryOptimizer.h"

using std::string;
using std::cout;
using std::endl;
using std::ostream;

test_dir_results GA2DBinaryOptimizer::results;

GA2DBinaryOptimizer::GA2DBinaryOptimizer() {
	// TODO Auto-generated constructor stub

}

GA2DBinaryOptimizer::~GA2DBinaryOptimizer() {
	// TODO Auto-generated destructor stub
}

uint GA2DBinaryOptimizer::optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, ostream& log/*= cout*/, uint mode/*= 0*/) {
	results = overall_results;
	int popsize = 300;
	int ngen = 6000;
	float pmut = 0.001;
	float pcross = 0.9;

	GA2DBinaryStringGenome genome(width, height, Objective);
	genome.crossover(CustomCrossover);

	GASteadyStateGA ga(genome);
	ga.populationSize(popsize);
	ga.nGenerations(ngen);
	ga.pMutation(pmut);
	ga.pCrossover(pcross);
	ga.pReplacement(0.5);

	ga.evolve();

	log << "Best score: " << ga.statistics().maxEver() << endl;
	GA2DBinaryStringGenome & best_genome = (GA2DBinaryStringGenome &)ga.statistics().bestIndividual();
	log << "Best solution found:\n" << best_genome << endl;
	solution->dim2 = (bool**)malloc(sizeof(bool*)*height);
	for (int i = 0; i < best_genome.height(); i++) {
		solution->dim2[i] = (bool*)malloc(sizeof(bool)*width);
	}
	for (uint i = 0; i < height; i++) {
		for (uint j = 0; j < width; j++) {
			solution->dim2[i][j] = best_genome.gene(j,i);
		}
	}
	solution->height = height;
	solution->width = width;
	return 0;
}

float GA2DBinaryOptimizer::Objective(GAGenome& g) {
	GA2DBinaryStringGenome & genome = (GA2DBinaryStringGenome &)g;

	float score = 0;
	for (uint i = 0; i < results.size(); i++) {
		string file_proto_name = results[i].first;
		for (uint j = 0; j < results[i].second.size(); j++) {
			for (uint k = 0; k < results[i].second[j].size(); k++) {
				string proto_name = std::get<0>(results[i].second[j].at(k));
				float sim = std::get<2>(results[i].second[j].at(k));
				uint x = k % genome.width();
				uint y = k / genome.width();
				if (genome.gene(x,y)) {
					if (file_proto_name.compare(0,file_proto_name.length(),proto_name,0,file_proto_name.length())) {
						if (sim > 0.8) score -= sim;
						else score += 1 - sim;
					} else {
						if (sim > 0.8) score += sim;
						else score -= 1 - sim;
					}
				}
			}
		}
	}
	return (score > 0) ? score : 0;
}

int GA2DBinaryOptimizer::CustomCrossover(const GAGenome& p1, const GAGenome& p2, GAGenome* c1, GAGenome* c2) {
	GA2DBinaryStringGenome & mom = (GA2DBinaryStringGenome &)p1;
	GA2DBinaryStringGenome & dad = (GA2DBinaryStringGenome &)p2;

	if (!c1) return 0;
	GA2DBinaryStringGenome & child = (GA2DBinaryStringGenome &)*c1;
	for (int row = 0; row < mom.height(); row++) {
		uint rnd = GARandomInt(0, mom.width());
		child.copy(mom,0,row,0,row,rnd,1);
		child.copy(dad,rnd,row,rnd,row,dad.width()-rnd,1);
	}
	return 1;
}
