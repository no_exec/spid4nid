/*
 * GA1DBinaryOptimizer.cpp
 *
 *  Created on: 28.9.2014
 *      Author: adam
 */

#include <tuple>
#include <iostream>
#include "GA1DBinaryOptimizer.h"

using std::string;
using std::cout;
using std::endl;
using std::ostream;

test_dir_results GA1DBinaryOptimizer::results;

GA1DBinaryOptimizer::GA1DBinaryOptimizer() {
	// TODO Auto-generated constructor stub

}

GA1DBinaryOptimizer::~GA1DBinaryOptimizer() {
	// TODO Auto-generated destructor stub
}

uint GA1DBinaryOptimizer::optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, ostream& log/*= cout*/, uint mode/*= 0*/) {
	results = overall_results;
	int popsize = 200;
	int ngen = 4000;
	float pmut = 0.01;
	float pcross = 0.9;
	GA1DBinaryStringGenome genome(width, Objective);

	GASimpleGA ga(genome);
	ga.populationSize(popsize);
	ga.nGenerations(ngen);
	ga.pMutation(pmut);
	ga.pCrossover(pcross);

	ga.evolve();

	GA1DBinaryStringGenome & best_genome = (GA1DBinaryStringGenome &)ga.statistics().bestIndividual();
	log << "Best solution found:\n" << best_genome << endl;
	log << "Max score: " << ga.statistics().maxEver() << endl;
	solution->dim1 = (bool*)malloc(sizeof(bool)*width);
	for (uint i = 0; i < width; i++) {
		solution->dim1[i] = best_genome.gene(i);
	}
	solution->width = width;
	return 0;
}

float GA1DBinaryOptimizer::Objective(GAGenome& g) {
	GA1DBinaryStringGenome & genome = (GA1DBinaryStringGenome &)g;

	float score = 0;
	for (uint i = 0; i < results.size(); i++) {
		string file_proto_name = results[i].first;
		for (uint j = 0; j < results[i].second.size(); j++) {
			for (uint k = 0; k < results[i].second[j].size(); k++) {
				string proto_name = std::get<0>(results[i].second[j].at(k));
				float sim = std::get<2>(results[i].second[j].at(k));
				uint x = k % genome.size();
				if (genome.gene(x)) {
					if (file_proto_name.compare(0,file_proto_name.length(),proto_name,0,file_proto_name.length())) {
						if (sim > 0.8) score -= sim;
						else score += 1 - sim;
						
					} else {
						
						if (sim > 0.8) score += sim;
						else score -= 1 - sim;
					}
				}
			}
		}
	}
	return (score > 0) ? score : 0;
}
