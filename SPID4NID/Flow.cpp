/*
 * Flow.cpp
 *
 *  Created on: 18.3.2014
 *      Author: adam
 */

#include "Flow.h"

Flow::Flow(): m_id(0), m_dir_changes(0), m_packet_num(0), m_biflow(false),
m_state(Flow::inactive), m_data_sizes(0), m_last_dir(true), m_last_size(-1) {
	// TODO Auto-generated constructor stub

}

Flow::Flow(uint id, uint dir_changes, uint num_packets, bool biflow):
	m_id(id), m_dir_changes(dir_changes), m_packet_num(num_packets),
	m_biflow(biflow), m_state(Flow::S), m_data_sizes(0), m_last_dir(true), m_last_size(-1) {

}

Flow::~Flow() {
	// TODO Auto-generated destructor stub
}

