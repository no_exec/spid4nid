/*
 * data_types.h
 *
 *  Created on: 18.3.2014
 *      Author: adam
 */

#ifndef DATA_TYPES_H_
#define DATA_TYPES_H_

#include <string>
#include <tuple>
#include <vector>
#include <map>

typedef unsigned int uint;
typedef unsigned char uchar;
typedef std::vector<std::tuple<std::string,uint,float>> classif_result;
typedef std::vector<classif_result> test_results;
typedef std::vector<std::pair<std::string,test_results> > test_dir_results;

typedef struct config {
	bool* dim1 = NULL;
	bool** dim2 = NULL;
	unsigned int width = 0;
	unsigned int height = 0;
} configuration;

typedef struct pckt {
	uchar* data;
	bool direction;
	uint length;
	uint order;
	uchar flags;
	uint seq;
	uint ack;
	bool keep_stored;
} packet_info;

typedef struct attr_fprint {
	std::vector<uint> counter_vec;
	std::vector<float> probability_vec;
} attr_fingerprint;

typedef std::map<std::string, std::map<uint, attr_fingerprint> > proto_fprints;

#endif /* DATA_TYPES_H_ */
