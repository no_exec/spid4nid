/*
 * XMLSerializer.cpp
 *
 *  Created on: 3.2.2015
 *      Author: adam
 */

#include "XMLSerializer.h"

#define FPRINTS 1
#define CONFIG 2

XMLSerializer::XMLSerializer() {
	// TODO Auto-generated constructor stub

}

XMLSerializer::~XMLSerializer() {
	// TODO Auto-generated destructor stub
}

int XMLSerializer::storeState(Classifier* classifier, const char* out_file, uint mode) {
	if (!mode) mode = FPRINTS; //default
	XMLDocument* doc = new XMLDocument();
	XMLNode* decl = doc->InsertFirstChild(doc->NewDeclaration());
	XMLNode* root = doc->InsertEndChild(
			doc->NewElement("program_state"));
	if (mode & FPRINTS) {
		this->modelFingerprints(classifier->getProtocolFingerprints(), doc, root);
	}
	if (mode & CONFIG) {
		this->modelConfiguration(classifier->getConfiguration(), doc, root);
	}
	doc->SaveFile(out_file);
	delete doc;
	return 0;
}

int XMLSerializer::loadState(Classifier* classifier, const char* in_file) {
	XMLDocument* doc = new XMLDocument();
	doc->LoadFile(in_file);
	XMLElement* root = doc->RootElement();
	XMLElement* proto = root->FirstChildElement("protocol_fingerprints");
	if (proto) {
		proto_fprints fprints;
		this->parseFingerprints(&fprints, proto);
		classifier->setProtocolFingerprints(fprints);
	}
	XMLElement * config_elem = root->FirstChildElement("configuration");
	if (config_elem) {
		configuration config;
		this->parseConfiguration(&config, config_elem);
		classifier->setConfiguration(config);
	}
	delete doc;
	return 0;
}

int XMLSerializer::modelFingerprints(proto_fprints* fingerprints, XMLDocument* doc,
		XMLNode* root) {
	XMLNode* protocol_fprints = root->InsertEndChild(
			doc->NewElement("protocol_fingerprints"));
	proto_fprints::iterator iter1;
	std::map<uint, attr_fingerprint>::iterator iter2;
	for (iter1 = fingerprints->begin(); iter1 != fingerprints->end(); iter1++) {
		XMLElement* proto = (protocol_fprints->InsertEndChild(
				doc->NewElement("protocol")))->ToElement();
		proto->SetAttribute("name", iter1->first.c_str());
		for (iter2 = iter1->second.begin(); iter2 != iter1->second.end();
				iter2++) {
			XMLElement* feature = (proto->InsertEndChild(
					doc->NewElement("feature")))->ToElement();
			feature->SetAttribute("id", iter2->first);
			XMLElement* vector = (feature->InsertEndChild(
					doc->NewElement("vector")))->ToElement();
			vector->SetAttribute("type", "counter");
			for (uint i = 0; i < iter2->second.counter_vec.size(); i++) {
				XMLElement* value = (vector->InsertEndChild(
						doc->NewElement("v")))->ToElement();
				value->SetText(iter2->second.counter_vec[i]);
			}
			vector =
					(feature->InsertEndChild(doc->NewElement("vector")))->ToElement();
			vector->SetAttribute("type", "probability");
			for (uint i = 0; i < iter2->second.probability_vec.size(); i++) {
				XMLElement* value = (vector->InsertEndChild(
						doc->NewElement("v")))->ToElement();
				value->SetText(iter2->second.probability_vec[i]);
			}
		}
	}
	return 0;
}

int XMLSerializer::parseFingerprints(proto_fprints* fingerprints, XMLElement* root) {
	XMLElement* proto_ele = root->FirstChildElement();
	while (proto_ele) {
		std::map<uint, attr_fingerprint> feature_map;
		XMLElement* feature = proto_ele->FirstChildElement();
		while (feature) {
			attr_fingerprint fprints;
			XMLElement* vector1 = feature->FirstChildElement();
			XMLElement* vector2 = feature->LastChildElement();
			XMLElement* value = vector1->FirstChildElement();
			while (value) {
				unsigned int val = 0;
				value->QueryUnsignedText(&val);
				fprints.counter_vec.push_back(val);
				value = value->NextSiblingElement();
			}
			value = vector2->FirstChildElement();
			while (value) {
				float val = 0.0f;
				value->QueryFloatText(&val);
				fprints.probability_vec.push_back(val);
				value = value->NextSiblingElement();
			}
			unsigned int id = 0;
			feature->QueryUnsignedAttribute("id", &id);
			feature_map[id] = fprints;
			feature = feature->NextSiblingElement();
		}
		const char* proto_name = proto_ele->Attribute("name");
		(*fingerprints)[proto_name] = feature_map;
		proto_ele = proto_ele->NextSiblingElement();
	}
	return 0;
}

int XMLSerializer::modelConfiguration(configuration config, XMLDocument* doc,
		XMLNode* root) {
	XMLElement* conf =
			(root->InsertEndChild(doc->NewElement("configuration")))->ToElement();
	if (config.dim1) {
		conf->SetAttribute("type", "1d");
		conf->SetAttribute("length", config.width);
		for (uint i = 0; i < config.width; i++) {
			XMLElement* value =
					conf->InsertEndChild(doc->NewElement("v"))->ToElement();
			value->SetText(config.dim1[i]);
		}
	} else if (config.dim2) {
		conf->SetAttribute("type", "2d");
		conf->SetAttribute("width", config.width);
		conf->SetAttribute("height", config.height);
		for (uint i = 0; i < config.height; i++) {
			for (uint j = 0; j < config.width; j++) {
				XMLElement* value =
						conf->InsertEndChild(doc->NewElement("v"))->ToElement();
				value->SetText(config.dim2[i][j]);
			}
		}
	}
	return 0;
}

int XMLSerializer::parseConfiguration(configuration* config,
		XMLElement* root) {
	if (root->Attribute("type", "1d")) {
		unsigned int len = 0;
		root->QueryUnsignedAttribute("length", &len);
		config->dim1 = new bool[len];
		config->width = len;
		XMLElement* val_elem = root->FirstChildElement();
		unsigned int c = 0;
		while (val_elem) {
			bool val = false;
			val_elem->QueryBoolText(&val);
			config->dim1[c++] = val;
			val_elem = val_elem->NextSiblingElement();
		}
	} else if (root->Attribute("type", "2d")) {
		unsigned int width = 0, height = 0;
		root->QueryUnsignedAttribute("width", &width);
		root->QueryUnsignedAttribute("height", &height);
		config->dim2 = new bool*[height];
		for (uint i = 0; i < height; i++)
			config->dim2[i] = new bool[width];
		config->width = width;
		config->height = height;
		XMLElement* val_elem = root->FirstChildElement();
		unsigned int c = 0;
		while (val_elem) {
			bool val = false;
			val_elem->QueryBoolText(&val);
			config->dim2[c / width][c % width] = val;
			c++;
			val_elem = val_elem->NextSiblingElement();
		}
	}
	return 0;
}
