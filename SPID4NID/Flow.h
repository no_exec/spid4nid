/*
 * Flow.h
 *
 *  Created on: 18.3.2014
 *      Author: adam
 */

#ifndef FLOW_H_
#define FLOW_H_

#include <deque>
#include <map>
#include "definitions.h"

class Flow {
public:
	//struct flow_char m_flow_id;
	uint m_id;
	uint m_dir_changes;
	uint m_packet_num;
	uint m_data_sizes;
	uint m_last_size;
	bool m_last_dir;
	bool m_biflow;
	std::map<uint, attr_fingerprint> fingerprints;
	std::deque<packet_info> packet_buffer;
	enum State { S, SSA, SSAA, active, inactive };
	State m_state;

//public:
	Flow();
	Flow(uint id, uint dir_changes, uint num_packets, bool biflow);
	virtual ~Flow();
};

#endif /* FLOW_H_ */
