/*
 * Metric.h
 *
 *  Created on: 21.3.2014
 *      Author: adam
 */

#ifndef METRIC_H_
#define METRIC_H_

#include "definitions.h"

class Metric {
public:
	Metric();
	virtual ~Metric();

	virtual float computeSimilarity(attr_fingerprint protocol,
			attr_fingerprint protocol_model) = 0;
};

#endif /* METRIC_H_ */
