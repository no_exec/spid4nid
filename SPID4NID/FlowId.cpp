/*
 * FlowId.cpp
 *
 *  Created on: 23.3.2014
 *      Author: adam
 */

#include "FlowId.h"

FlowId::FlowId() : m_src_ip(""), m_dst_ip(""), m_src_port(0),
				   m_dst_port(0), m_protocol(0) {
	// TODO Auto-generated constructor stub

}

FlowId::FlowId(std::string src_ip, std::string dst_ip, uint src_port,
		uint dst_port, uchar proto) : m_src_ip(src_ip), m_dst_ip(dst_ip),
				m_src_port(src_port), m_dst_port(dst_port), m_protocol(proto) {

}

FlowId::~FlowId() {
	// TODO Auto-generated destructor stub
}

bool FlowId::operator==(const FlowId& flow) const {
	if (m_protocol != flow.m_protocol) return false;

	if (m_src_ip == flow.m_src_ip && m_dst_ip == flow.m_dst_ip
		&& m_src_port == flow.m_src_port
		&& m_dst_port == flow.m_dst_port) return true;

	if (m_src_ip == flow.m_dst_ip && m_dst_ip == flow.m_src_ip
		&& m_src_port == flow.m_dst_port
		&& m_dst_port == flow.m_src_port) return true;

	return false;
}
