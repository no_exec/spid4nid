/*
 * FlowId.h
 *
 *  Created on: 23.3.2014
 *      Author: adam
 */

#ifndef FLOWID_H_
#define FLOWID_H_

#include <functional>
#include <cstddef>
#include "definitions.h"

class FlowId {
public:
	std::string m_src_ip;
	std::string m_dst_ip;
	uint m_src_port;
	uint m_dst_port;
	uchar m_protocol;

	FlowId();
	FlowId(std::string src_ip, std::string dst_ip, uint src_port,
			uint dst_port, uchar proto);
	virtual ~FlowId();

	bool operator==(const FlowId& flow) const;
};

struct FlowIdHasher {
	size_t operator()(const FlowId& flow) const {
	    using std::size_t;
	    using std::hash;
	    using std::string;

	    return (hash<string>()(flow.m_src_ip)
	    		^ hash<string>()(flow.m_dst_ip)
	    		^ hash<uint>()(flow.m_src_port)
	    		^ hash<uint>()(flow.m_dst_port)
	    		^ hash<uchar>()(flow.m_protocol));
	}
};
/*
struct FlowIdEqual {
	bool operator()(const FlowId& flow1, const FlowId& flow2) {
		if (flow1.m_protocol != flow2.m_protocol) return false;

		if (flow1.m_src_ip == flow2.m_src_ip
			&& flow1.m_dst_ip == flow2.m_dst_ip
			&& flow1.m_src_port == flow2.m_src_port
			&& flow1.m_dst_port == flow2.m_dst_port) return true;

		if (flow1.m_src_ip == flow2.m_dst_ip
			&& flow1.m_dst_ip == flow2.m_src_ip
			&& flow1.m_src_port == flow2.m_dst_port
			&& flow1.m_dst_port == flow2.m_src_port) return true;

		return false;
	}
};
*/
/*
struct compare_flows {
	bool operator ()(FlowId const& flow1, FlowId const& flow2) {
		if (flow1.m_protocol != flow2.m_protocol) return true;

		if (flow1.m_src_ip == flow2.m_src_ip && flow1.m_dst_ip == flow2.m_dst_ip
			&& flow1.m_src_port == flow2.m_src_port && flow1.m_dst_port == flow2.m_dst_port)
			return false;
		if (flow1.m_src_ip == flow2.m_dst_ip && flow1.m_dst_ip == flow2.m_src_ip
			&& flow1.m_src_port == flow2.m_dst_port && flow1.m_dst_port == flow2.m_src_port)
				return false;
		else return true;
	}
};
*/
#endif /* FLOWID_H_ */
