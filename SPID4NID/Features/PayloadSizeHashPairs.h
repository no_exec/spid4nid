/*
 * PayloadSizeHashPairs.h
 *
 *  Created on: 18.11.2014
 *      Author: adam
 */

#ifndef PAYLOADSIZEHASHPAIRS_H_
#define PAYLOADSIZEHASHPAIRS_H_

#include "Feature.h"

class PayloadSizeHashPairs: public Feature {
private:
	const uint THRESH = 8;
public:
	PayloadSizeHashPairs(uint id);
	virtual ~PayloadSizeHashPairs();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* PAYLOADSIZEHASHPAIRS_H_ */
