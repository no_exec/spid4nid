/*
 * PayloadSizeChangesFeature.h
 *
 *  Created on: 14.11.2015
 *      Author: adam
 */

#ifndef PAYLOADSIZECHANGESFEATURE_H_
#define PAYLOADSIZECHANGESFEATURE_H_

#include "Feature.h"

class PayloadSizeChangesFeature : public Feature {
private:
	const uint THRESH = 16;
public:
	PayloadSizeChangesFeature(uint id);
	virtual ~PayloadSizeChangesFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* PAYLOADSIZECHANGESFEATURE_H_ */
