/*
 * Feature.h
 *
 *  Created on: 18.3.2014
 *      Author: adam
 */

#ifndef FEATURE_H_
#define FEATURE_H_

#include "../definitions.h"
#include "../Flow.h"

#define SUCCESS 0
#define HUNGRY 1
#define FAILURE 2

class Feature {
public:
	uint m_id;
	bool m_merge;
	Feature(uint id);
	virtual ~Feature();

	virtual uint updateFeature(Flow* p_flow) = 0;
	virtual attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2) = 0;

};

#endif /* FEATURE_H_ */
