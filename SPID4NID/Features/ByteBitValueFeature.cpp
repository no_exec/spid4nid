/*
 * ByteBitValueFeature.cpp
 *
 *  Created on: 8.11.2015
 *      Author: adam
 */

#include "ByteBitValueFeature.h"

#define VEC_LEN 256

using std::vector;

ByteBitValueFeature::ByteBitValueFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
}

ByteBitValueFeature::~ByteBitValueFeature() {
	// TODO Auto-generated destructor stub
}

uint ByteBitValueFeature::updateFeature(Flow* p_flow) {
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESHOLD)
		return SUCCESS;
#endif
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	uint numBytes = packet.length > 32 ? 32 : packet.length;
	if (numBytes == 0) return (p_flow->m_packet_num < THRESHOLD) ? HUNGRY : SUCCESS;
	int* zeroCounters = new int[8];
	int offset = packet.direction ? 8 : 0;
	for (uint i = 0; i < numBytes; i++) {
		uchar cur_byte = packet.data[i];
		for (uint j = 0; j < 8; j++) {
			if (cur_byte & 1) zeroCounters[j]++;
			cur_byte >>= 1;
		}
		for (int i = 0; i < 8; i++) {
			int index = 16 * (offset + i) + ((zeroCounters[i] * 15) / numBytes);
			p_attr->counter_vec[index % VEC_LEN]++;
		}
	}
	int c = 0;
	for (int i = 0; i < VEC_LEN; i++) {
		c += p_attr->counter_vec[i];
	}
	for (int i = 0; i < VEC_LEN; i++) {
		p_attr->probability_vec[i] = (float)p_attr->counter_vec[i] / (float)(c);
	}
	if (p_flow->m_packet_num < THRESHOLD) return HUNGRY;
	else return SUCCESS;
}

attr_fingerprint ByteBitValueFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
