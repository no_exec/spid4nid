/*
 * FirstPacketPerDirectionNibblesFeature.cpp
 *
 *  Created on: 8.11.2015
 *      Author: adam
 */

#include "FirstPacketPerDirectionNibblesFeature.h"

using std::vector;

#define VEC_LEN 256

FirstPacketPerDirectionNibblesFeature::FirstPacketPerDirectionNibblesFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
}

FirstPacketPerDirectionNibblesFeature::~FirstPacketPerDirectionNibblesFeature() {
	// TODO Auto-generated destructor stub
}

uint FirstPacketPerDirectionNibblesFeature::updateFeature(Flow* p_flow) {
	packet_info packet = p_flow->packet_buffer.front();
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	bool clientToServer = false, serverToClient = false;
	if (p_flow->m_dir_changes > 2) {
		clientToServer = serverToClient = true;
	} else if (p_flow->m_dir_changes == 1) {
		clientToServer = true;
	}
	if (clientToServer && serverToClient) return SUCCESS;
	if (packet.length == 0) {
		if (clientToServer && serverToClient)
			return SUCCESS;
		else
			return HUNGRY;
	}
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	if (!clientToServer) {
		uint numBytes = packet.length < 8 ? packet.length : 8;
		for (uint i = 0; i < numBytes; i++) {
			uchar nibble = (packet.data[i]^(packet.data[i]>>4)) & 0x0f;
			uint index = i * 16 + nibble;
			p_attr->counter_vec[index]++;
		}
		clientToServer = true;
	} else if (!serverToClient) {
		uint numBytes = packet.length < 8 ? packet.length : 8;
		for (uint i = 0; i < numBytes; i++) {
			uchar nibble = (packet.data[i]^(packet.data[i]>>4)) & 0x0f;
			uint index = VEC_LEN/2 + i * 16 + nibble;
			p_attr->counter_vec[index]++;
		}
		clientToServer = true;
	}
	uint count = 0;
	for (int i = 0; i < VEC_LEN; i++) {
		count += p_attr->counter_vec[i];
	}
	if (count) {
		for (int i = 0; i < VEC_LEN; i++) {
			p_attr->probability_vec[i] = (float)(p_attr->counter_vec[i])/(float)count;
		}
	}
	if (clientToServer && serverToClient) return SUCCESS;
	else return HUNGRY;
}

attr_fingerprint FirstPacketPerDirectionNibblesFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
