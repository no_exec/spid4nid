/*
 * DirectionChangesFeature.cpp
 *
 *  Created on: 31.7.2014
 *      Author: adam
 */

#include <vector>
#include "DirectionChangesFeature.h"

#define VEC_LEN 2

using std::vector;

DirectionChangesFeature::DirectionChangesFeature(uint id) :
		Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 16;
}

DirectionChangesFeature::~DirectionChangesFeature() {
	// TODO Auto-generated destructor stub
}

uint DirectionChangesFeature::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH)
		return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	if (p_flow->m_packet_num > 1) {
		uint sum = p_attr->counter_vec[0] + p_attr->counter_vec[1];
		if (p_flow->m_last_dir == packet.direction) {
			p_attr->counter_vec[0]++;
		} else {
			p_attr->counter_vec[1]++;
		}
		sum++;
		p_attr->probability_vec[0] = (float) p_attr->counter_vec[0]
				/ (float) sum;
		p_attr->probability_vec[1] = (float) p_attr->counter_vec[1]
				/ (float) sum;
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint DirectionChangesFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
