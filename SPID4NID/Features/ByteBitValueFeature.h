/*
 * ByteBitValueFeature.h
 *
 *  Created on: 8.11.2015
 *      Author: adam
 */

#ifndef BYTEBITVALUEFEATURE_H_
#define BYTEBITVALUEFEATURE_H_

#include "Feature.h"

class ByteBitValueFeature : public Feature {
private:
	const uint THRESHOLD = 8;
public:
	ByteBitValueFeature(uint id);
	virtual ~ByteBitValueFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* BYTEBITVALUEFEATURE_H_ */
