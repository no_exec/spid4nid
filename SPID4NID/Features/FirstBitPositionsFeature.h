/*
 * FirstBitPositionsFeature.h
 *
 *  Created on: 21.10.2015
 *      Author: adam
 */

#ifndef FIRSTBITPOSITIONSFEATURE_H_
#define FIRSTBITPOSITIONSFEATURE_H_

#include "Feature.h"

class FirstBitPositionsFeature : public Feature {
private:
	static const uint THRESHOLD = 32;
public:
	FirstBitPositionsFeature(uint id);
	virtual ~FirstBitPositionsFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* FIRSTBITPOSITIONSFEATURE_H_ */
