/*
 * ByteVariance.cpp
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#include "ByteVariance.h"
#include <cmath>

#define COUNT_LEN 256
#define PROB_LEN 2

using std::vector;

ByteVariance::ByteVariance(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 96;
	m_merge = true;
}

ByteVariance::~ByteVariance() {
	// TODO Auto-generated destructor stub
}

uint ByteVariance::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(COUNT_LEN, 0);
		attr.probability_vec = vector<float>(PROB_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
#ifdef REPLETE
	if (p_flow->m_data_sizes - packet.length > THRESH)
		return SUCCESS;
#endif
	if (packet.length == 0) return (p_flow->m_data_sizes > THRESH) ? SUCCESS : HUNGRY;
	for (uint i = 0; i < packet.length; i++) {
		p_attr->counter_vec[packet.data[i]]++;
	}
	uint sum = 0;
	for (uint i = 0; i < COUNT_LEN; i++) {
		sum += i * p_attr->counter_vec[i];
	}
	float mean = (float)sum / (float)p_flow->m_data_sizes;
	float var = 0.0f;
	uint min = COUNT_LEN, max = 0;
	for (uint i = 0; i < COUNT_LEN; i++) {
		var += p_attr->counter_vec[i] * pow(((float)i - mean),2.0);
		if (p_attr->counter_vec[i] && i < min) min = i;
		if (p_attr->counter_vec[i] && i > max) max = i;
	}
	var /= (float)p_flow->m_data_sizes;
	float max_var = pow((max - min),2.0)/4;
	p_attr->probability_vec[0] = var/max_var;
	p_attr->probability_vec[1] = 1 - p_attr->probability_vec[0];
	return (p_flow->m_data_sizes < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint ByteVariance::mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	attr.counter_vec = vector<uint>(COUNT_LEN, 0);
	attr.probability_vec = vector<float>(PROB_LEN, 0.0f);
	uint s = 0, c = 0;
	for (uint i = 0; i < COUNT_LEN; i++) {
		attr.counter_vec[i] = fpr1.counter_vec[i] + fpr2.counter_vec[i];
		c += attr.counter_vec[i];
		s += i * attr.counter_vec[i];
	}
	float mean = (float)s / (float)c;
	float var = 0.0f;
	uint min = COUNT_LEN, max = 0;
	for (uint i = 0; i < COUNT_LEN; i++) {
		var += attr.counter_vec[i] * pow(((float)i - mean),2.0);
		if (attr.counter_vec[i] && i < min) min = i;
		if (attr.counter_vec[i] && i > max) max = i;
	}
	var /= (float)c;
	float max_var = pow((max - min),2.0)/4;
	attr.probability_vec[0] = var/max_var;
	attr.probability_vec[1] = 1 - attr.probability_vec[0];
	return attr;
}
