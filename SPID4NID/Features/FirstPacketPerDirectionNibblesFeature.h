/*
 * FirstPacketPerDirectionNibblesFeature.h
 *
 *  Created on: 8.11.2015
 *      Author: adam
 */

#ifndef FIRSTPACKETPERDIRECTIONNIBBLESFEATURE_H_
#define FIRSTPACKETPERDIRECTIONNIBBLESFEATURE_H_

#include "Feature.h"

class FirstPacketPerDirectionNibblesFeature : public Feature {

public:
	FirstPacketPerDirectionNibblesFeature(uint id);
	virtual ~FirstPacketPerDirectionNibblesFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* FIRSTPACKETPERDIRECTIONNIBBLESFEATURE_H_ */
