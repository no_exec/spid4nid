/*
 * NibblePositionFrequencyFeature.cpp
 *
 *  Created on: 17.11.2015
 *      Author: adam
 */

#include "NibblePositionFrequencyFeature.h"

#define VEC_LEN 256

using std::vector;

NibblePositionFrequencyFeature::NibblePositionFrequencyFeature(uint id) :
		Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 8;
}

NibblePositionFrequencyFeature::~NibblePositionFrequencyFeature() {
	// TODO Auto-generated destructor stub
}

uint NibblePositionFrequencyFeature::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH)
		return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	if (p_flow->m_packet_num >= THRESH)
		return SUCCESS;
	uint len = (packet.length < 16) ? packet.length : 16;
	if (len == 0)
		return HUNGRY;
	for (uint nibbleIndex = 0; nibbleIndex < 2 * len; nibbleIndex++) {
		uint byteIndex = nibbleIndex / 2;
		uchar nibble;
		if (nibbleIndex % 2) {
			nibble = packet.data[byteIndex] >> 4;
		} else {
			nibble = packet.data[byteIndex] & 0x0f;
		}
		p_attr->counter_vec[nibbleIndex * 16 + nibble]++;
	}
	int count = 0;
	for (int i = 0; i < VEC_LEN; i++) {
		count += p_attr->counter_vec[i];
	}
	for (int i = 0; i < VEC_LEN; i++) {
		p_attr->probability_vec[i] = p_attr->counter_vec[i] / (float) count;
	}
	return (p_flow->m_packet_num >= THRESH) ? SUCCESS : HUNGRY;
}

attr_fingerprint NibblePositionFrequencyFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
