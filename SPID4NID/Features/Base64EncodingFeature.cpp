/*
 * Base64EncodingFeature.cpp
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#include <cctype>
#include "Base64EncodingFeature.h"

#define VEC_LEN 2

using std::vector;

Base64EncodingFeature::Base64EncodingFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 32;
}

Base64EncodingFeature::~Base64EncodingFeature() {
	// TODO Auto-generated destructor stub
}

uint Base64EncodingFeature::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
#ifdef REPLETE
	if (p_flow->m_data_sizes - packet.length >= THRESH)
		return SUCCESS;
#endif
	bool base64 = true;
	uint i = 0;
	while (base64 && i < packet.length) {
		uchar c = packet.data[i++];
		if (!isalnum(c) && c != '+' && c != '/' && c != '=') {
			base64 = false;
		}
	}
	if (!base64) p_attr->counter_vec[0] = 1;
	if (p_attr->counter_vec[0]) {
		p_attr->probability_vec[0] = 1.0;
		p_attr->probability_vec[1] = 0.0;
	} else {
		p_attr->counter_vec[1] = 1;
		p_attr->probability_vec[0] = 0.0;
		p_attr->probability_vec[1] = 1.0;
	}
	return (p_flow->m_data_sizes < 32) ? HUNGRY : SUCCESS;
}

attr_fingerprint Base64EncodingFeature::mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
