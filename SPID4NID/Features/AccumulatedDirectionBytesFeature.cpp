/*
 * AccumulatedDirectionBytesFeature.cpp
 *
 *  Created on: 9.4.2014
 *      Author: adam
 */

#include <vector>
#include "AccumulatedDirectionBytesFeature.h"

#define VEC_LEN 256

using std::vector;

AccumulatedDirectionBytesFeature::AccumulatedDirectionBytesFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 8;
}

AccumulatedDirectionBytesFeature::~AccumulatedDirectionBytesFeature() {
	// TODO Auto-generated destructor stub
}

uint AccumulatedDirectionBytesFeature::updateFeature(Flow* p_flow) {
	packet_info packet = p_flow->packet_buffer.front();
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_dir_changes > THRESH) return SUCCESS;
#endif
	uint retcode = (p_flow->m_dir_changes < THRESH) ? HUNGRY : SUCCESS;
	if (packet.length == 0) return retcode;
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	p_attr->counter_vec[p_flow->m_dir_changes % VEC_LEN] += packet.length;

	uint count = p_flow->m_data_sizes;
	if (count) {
		for (int i = 0; i < VEC_LEN; i++) {
			p_attr->probability_vec[i] = (float)p_attr->counter_vec[i]/(float)count;
		}
	}
	return retcode;
}

attr_fingerprint AccumulatedDirectionBytesFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
