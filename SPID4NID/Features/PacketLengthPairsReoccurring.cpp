/*
 * PacketLengthPairsReoccurring.cpp
 *
 *  Created on: 18.11.2014
 *      Author: adam
 */

#include "PacketLengthPairsReoccurring.h"

#define VEC_LEN 1502

using std::vector;

PacketLengthPairsReoccurring::PacketLengthPairsReoccurring(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 8;
	m_merge = true;
}

PacketLengthPairsReoccurring::~PacketLengthPairsReoccurring() {
	// TODO Auto-generated destructor stub
}

uint PacketLengthPairsReoccurring::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num >= THRESH)
		return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	if (p_flow->m_packet_num == 1) {
		p_attr->counter_vec[1501] = 1;
		p_attr->probability_vec[1501] = 1.0;
		return HUNGRY;
	}
	if (packet.length == p_flow->m_last_size) {
		p_attr->counter_vec[p_flow->m_last_size % VEC_LEN]++;
		uint c =0;
		for (int i = 0; i < VEC_LEN; i++) {
			c += p_attr->counter_vec[i];
		}
		for (int i = 0; i < VEC_LEN - 1; i++) {
			p_attr->probability_vec[i] = p_attr->counter_vec[i]/(float)c;
		}
		p_attr->counter_vec[1501] = 0;
		p_attr->probability_vec[1501] = 0.0f;
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint PacketLengthPairsReoccurring::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	attr.counter_vec = vector<uint>(VEC_LEN, 0);
	attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
	uint c = 0;
	for (uint i = 0; i < VEC_LEN - 1; i++) {
		attr.counter_vec[i] = fpr1.counter_vec[i] + fpr2.counter_vec[i];
		c += attr.counter_vec[i];
	}
	if (c == 0) {
		attr.counter_vec[1501] = 1;
		attr.probability_vec[1501] = 1.0f;
		return attr;
	}
	for (uint i = 0; i < VEC_LEN - 1; i++) {
		attr.probability_vec[i] = attr.counter_vec[i]/(float)c;
	}
	return attr; // currently not implemented
}
