/*
 * NewLineEqualityFeature.cpp
 *
 *  Created on: 10.4.2014
 *      Author: adam
 */

#include <vector>
#include "NewLineEqualityFeature.h"

using std::vector;

#define VEC_LEN 2

NewLineEqualityFeature::NewLineEqualityFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 8;
}

NewLineEqualityFeature::~NewLineEqualityFeature() {
	// TODO Auto-generated destructor stub
}

uint NewLineEqualityFeature::updateFeature(Flow* p_flow) {
	packet_info packet = p_flow->packet_buffer.front();
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH)
		return SUCCESS;
#endif
	if (packet.length == 0) return (p_flow->m_packet_num >= THRESH) ? SUCCESS : HUNGRY;
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	bool r = false;
	for (uint i = 0; i < packet.length; i++) {
		if (packet.data[i] == '\n') {
			if (!r) {
				p_attr->counter_vec[1] = 1;
			}
		} else {
			if (r) {
				p_attr->counter_vec[1] = 1;
			}
		}
		if (packet.data[i] == '\r') {
			r = true;
		} else {
			r = false;
		}
	}
	if (p_attr->counter_vec[1]) {
		p_attr->counter_vec[0] = 0;
		p_attr->probability_vec[0] = 0.0;
		p_attr->probability_vec[1] = 1.0;
	} else {
		p_attr->counter_vec[0] = 1;
		p_attr->probability_vec[0] = 1.0;
		p_attr->probability_vec[1] = 0.0;
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint NewLineEqualityFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
