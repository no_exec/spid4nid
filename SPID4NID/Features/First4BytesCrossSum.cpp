/*
 * First4BytesCrossSum.cpp
 *
 *  Created on: 2.8.2014
 *      Author: adam
 */

#include <vector>
#include "First4BytesCrossSum.h"

#define VEC_LEN 76

using std::vector;

First4BytesCrossSum::First4BytesCrossSum(uint id) :
		Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 2;
}

First4BytesCrossSum::~First4BytesCrossSum() {
	// TODO Auto-generated destructor stub
}

uint First4BytesCrossSum::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH) return SUCCESS;
#endif
	if (packet.length == 0)
		return (p_flow->m_packet_num < 2) ? HUNGRY : SUCCESS;

	int numBytes = (packet.length < 4) ? packet.length : 4;
	uint cross_sum = 0;
	for (int j = 0; j < numBytes; j++) {
		uchar byte = packet.data[j], rest = 0;
		do {
			rest = byte % 10;
			byte = byte / 10;
			cross_sum += rest;
		} while (byte > 0);
	}
	p_attr->counter_vec[cross_sum % VEC_LEN]++;

	for (uint i = 0; i < VEC_LEN; i++) {
		if (p_attr->counter_vec[i])
			p_attr->probability_vec[i] = (float) p_attr->counter_vec[i]
					/ (float) p_flow->m_packet_num;
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}
attr_fingerprint First4BytesCrossSum::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
