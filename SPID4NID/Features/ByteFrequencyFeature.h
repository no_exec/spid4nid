/*
 * ByteFrequencyFeature.h
 *
 *  Created on: 22.3.2014
 *      Author: adam
 */

#ifndef BYTEFREQUENCYFEATURE_H_
#define BYTEFREQUENCYFEATURE_H_

#include "Feature.h"


class ByteFrequencyFeature : public Feature {
private:
	const uint THRESH = 96;
public:
	ByteFrequencyFeature(uint id);
	virtual ~ByteFrequencyFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* BYTEFREQUENCYFEATURE_H_ */
