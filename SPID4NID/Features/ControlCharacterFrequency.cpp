/*
 * ControlCharacterFrequency.cpp
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#include <cctype>
#include "ControlCharacterFrequency.h"

#define VEC_LEN 34

using std::vector;

ControlCharacterFrequency::ControlCharacterFrequency(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 32;
}

ControlCharacterFrequency::~ControlCharacterFrequency() {
	// TODO Auto-generated destructor stub
}

uint ControlCharacterFrequency::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
#ifdef REPLETE
	if (p_flow->m_data_sizes - packet.length > THRESH)
		return SUCCESS;
#endif
	uchar c;
	if (packet.length == 0) return (p_flow->m_data_sizes < 32) ? HUNGRY : SUCCESS;
	for (uint i = 0; i < packet.length; i++) {
		c = packet.data[i];
		if (iscntrl(c)) {
			p_attr->counter_vec[(c != 127) ? c : 32]++;
		} else {
			p_attr->counter_vec[33]++;
		}
	}
	for (int i = 0; i < VEC_LEN; i++) {
		p_attr->probability_vec[i] = (float)p_attr->counter_vec[i]/(float)p_flow->m_data_sizes;
	}
	return (p_flow->m_data_sizes < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint ControlCharacterFrequency::mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
