/*
 * PayloadSizeFrequency.h
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#ifndef PAYLOADSIZEFREQUENCY_H_
#define PAYLOADSIZEFREQUENCY_H_

#include "Feature.h"

class PayloadSizeFrequency: public Feature {
private:
	const uint THRESH = 8;
public:
	PayloadSizeFrequency(uint id);
	virtual ~PayloadSizeFrequency();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* PAYLOADSIZEFREQUENCY_H_ */
