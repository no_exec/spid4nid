/*
 * FirstBitPositionsFeature.cpp
 *
 *  Created on: 21.10.2015
 *      Author: adam
 */

#include "FirstBitPositionsFeature.h"

#define VEC_LEN 64

using std::vector;

FirstBitPositionsFeature::FirstBitPositionsFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
}

FirstBitPositionsFeature::~FirstBitPositionsFeature() {
	// TODO Auto-generated destructor stub
}

uint FirstBitPositionsFeature::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	packet_info packet = p_flow->packet_buffer.front();
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	uint numBytes = packet.length > 4 ? 4 : packet.length;
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESHOLD) return SUCCESS;
#endif
	if (packet.length == 0) return HUNGRY;
	for (uint i = 0; i < numBytes; i++) {
		uchar ret = 0, cur_byte = packet.data[i];
		for (uint j = 0; j < 8; j++) {
			ret = (ret << 1) | (cur_byte & 1);
			cur_byte >>= 1;
		}
		for (uint k = 0; k < 8; k++) {
			uint offset = (i*8) + k;
			uint index = offset * 2 + (ret & 1);
			p_attr->counter_vec[index]++;
			ret >>= 1;
		}
	}
	uint count = 0;
	for (uint i = 0; i < VEC_LEN; i++) {
		count += p_attr->counter_vec[i];
	}
	for (uint i = 0; i < VEC_LEN; i++) {
		p_attr->probability_vec[i] = p_attr->counter_vec[i] / (float)count;
	}
	if (p_flow->m_packet_num < THRESHOLD)	return HUNGRY;
	else return SUCCESS;
}

attr_fingerprint FirstBitPositionsFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
