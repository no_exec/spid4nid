/*
 * PayloadSizeFrequency.cpp
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#include "PayloadSizeFrequency.h"

#define VEC_LEN 1501

using std::vector;

PayloadSizeFrequency::PayloadSizeFrequency(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 8;
}

PayloadSizeFrequency::~PayloadSizeFrequency() {
	// TODO Auto-generated destructor stub
}

uint PayloadSizeFrequency::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num >= THRESH)
		return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	p_attr->counter_vec[packet.length % VEC_LEN]++;
	for (int i = 0; i < VEC_LEN; i++) {
		p_attr->probability_vec[i] = (float)p_attr->counter_vec[i]/(float)p_flow->m_packet_num;
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint PayloadSizeFrequency::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
