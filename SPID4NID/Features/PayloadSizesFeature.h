/*
 * PayloadSizesFeature.h
 *
 *  Created on: 15.11.2015
 *      Author: adam
 */

#ifndef PAYLOADSIZESFEATURE_H_
#define PAYLOADSIZESFEATURE_H_

#include "Feature.h"

class PayloadSizesFeature : public Feature {
private:
	const uint THRESH = 16;
public:
	PayloadSizesFeature(uint id);
	virtual ~PayloadSizesFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* PAYLOADSIZESFEATURE_H_ */
