/*
 * DirectionByteFrequencyFeature.h
 *
 *  Created on: 9.4.2014
 *      Author: adam
 */

#ifndef DIRECTIONBYTEFREQUENCYFEATURE_H_
#define DIRECTIONBYTEFREQUENCYFEATURE_H_

#include "Feature.h"

class DirectionByteFrequencyFeature : public Feature {
private:
	const uint THRESH = 128;
public:
	DirectionByteFrequencyFeature(uint id);
	virtual ~DirectionByteFrequencyFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* DIRECTIONBYTEFREQUENCYFEATURE_H_ */
