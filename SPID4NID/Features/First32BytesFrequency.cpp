/*
 * First32BytesFrequency.cpp
 *
 *  Created on: 2.8.2014
 *      Author: adam
 */
#include <vector>
#include <numeric>
#include "First32BytesFrequency.h"

#define VEC_LEN 256

using std::vector;

First32BytesFrequency::First32BytesFrequency(uint id) :
		Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 16;
}

First32BytesFrequency::~First32BytesFrequency() {
	// TODO Auto-generated destructor stub
}

uint First32BytesFrequency::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH) return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	if (packet.length) {
		int len = (32 <= packet.length) ? 32 : packet.length;
		for (int j = 0; j < len; j++) {
			p_attr->counter_vec[packet.data[j]]++;
		}
		uint count = 0;
		for (int i = 0; i < VEC_LEN; i++) {
			count += p_attr->counter_vec[i];
		}
		for (int i = 0; i < VEC_LEN; i++) {

			p_attr->probability_vec[i] = (float) p_attr->counter_vec[i]
					/ (float) count;
		}
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint First32BytesFrequency::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
