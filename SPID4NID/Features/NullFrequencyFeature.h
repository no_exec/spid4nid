/*
 * NullFrequencyFeature.h
 *
 *  Created on: 9.4.2014
 *      Author: adam
 */

#ifndef NULLFREQUENCYFEATURE_H_
#define NULLFREQUENCYFEATURE_H_

#include "Feature.h"

class NullFrequencyFeature : public Feature {
private:
	const uint THRESH = 96;
public:
	NullFrequencyFeature(uint id);
	virtual ~NullFrequencyFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* NULLFREQUENCYFEATURE_H_ */
