/*
 * NibblePositionFrequencyFeature.h
 *
 *  Created on: 17.11.2015
 *      Author: adam
 */

#ifndef NIBBLEPOSITIONFREQUENCYFEATURE_H_
#define NIBBLEPOSITIONFREQUENCYFEATURE_H_

#include "Feature.h"

class NibblePositionFrequencyFeature : public Feature {
private:
	const uint THRESH = 8;
public:
	NibblePositionFrequencyFeature(uint id);
	virtual ~NibblePositionFrequencyFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* NIBBLEPOSITIONFREQUENCYFEATURE_H_ */
