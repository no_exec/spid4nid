/*
 * Entropy.cpp
 *
 *  Created on: 10.11.2014
 *      Author: adam
 */

#include "EntropyFeature.h"
#include <cmath>
#include <numeric>

#define COUNT_VEC_LEN 512
#define PROB_VEC_LEN 2

using std::vector;

EntropyFeature::EntropyFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 128;
	m_merge = true;
}

EntropyFeature::~EntropyFeature() {
	// TODO Auto-generated destructor stub
}

uint EntropyFeature::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(COUNT_VEC_LEN, 0);
		attr.probability_vec = vector<float>(PROB_VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	packet_info packet = p_flow->packet_buffer.front();
	uchar* packet_data = packet.data;
#ifdef REPLETE
	if (p_flow->m_data_sizes - packet.length > THRESH)
		return SUCCESS;
#endif
	if (packet.length == 0) return (p_flow->m_data_sizes < THRESH) ? HUNGRY : SUCCESS;
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	uint offset = packet.direction ? 0 : COUNT_VEC_LEN/2;
	for (uint i = 0; i < packet.length; i++) {
		p_attr->counter_vec[offset + packet_data[i]]++;
	}
	float prob = 0.0f;
	p_attr->probability_vec[packet.direction ? 1 : 0] = 0.0f;
	for (int i = 0; i < COUNT_VEC_LEN/2; i++) {
		prob = (float)p_attr->counter_vec[i + offset]/(float)p_flow->m_data_sizes;
		if (prob) p_attr->probability_vec[packet.direction ? 1 : 0] += prob * (-log(prob));
	}
	return (p_flow->m_data_sizes < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint EntropyFeature::mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	attr.counter_vec = vector<uint>(COUNT_VEC_LEN, 0);
	attr.probability_vec = vector<float>(PROB_VEC_LEN, 0.0f);
	uint sum = 0;
	for (int i = 0; i < COUNT_VEC_LEN; i++) {
		attr.counter_vec[i] = fpr1.counter_vec[i] + fpr2.counter_vec[i];
		sum += attr.counter_vec[i];
	}
	float prob = 0.0f;
	for (int i = 0; i < COUNT_VEC_LEN; i++) {
		prob = (float)attr.counter_vec[i] / (float)sum;
		if (prob) attr.probability_vec[(i >= COUNT_VEC_LEN/2) ? 1 : 0] += prob * (-log(prob));
	}
	return attr; // currently not implemented
}
