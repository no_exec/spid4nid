/*
 * DirectionEntropyFeature.h
 *
 *  Created on: 3.7.2014
 *      Author: adam
 */

#ifndef DIRECTIONENTROPYFEATURE_H_
#define DIRECTIONENTROPYFEATURE_H_

#include "Feature.h"

class DirectionEntropyFeature: public Feature {
private:
	const uint THRESH = 16;
public:
	DirectionEntropyFeature(uint id);
	virtual ~DirectionEntropyFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* DIRECTIONENTROPYFEATURE_H_ */
