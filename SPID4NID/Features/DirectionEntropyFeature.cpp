/*
 * DirectionEntropyFeature.cpp
 *
 *  Created on: 3.7.2014
 *      Author: adam
 */

#include <vector>
#include "DirectionEntropyFeature.h"

#define VEC_LEN 2

using std::vector;

DirectionEntropyFeature::DirectionEntropyFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 16;
}

DirectionEntropyFeature::~DirectionEntropyFeature() {
	// TODO Auto-generated destructor stub
}

uint DirectionEntropyFeature::updateFeature(Flow* p_flow) {
	packet_info packet = p_flow->packet_buffer.front();
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH)
		return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	p_attr->counter_vec[packet.direction ? 0 : 1]++;
	uint count = p_attr->counter_vec[0] + p_attr->counter_vec[1];
	p_attr->probability_vec[0] = (float)p_attr->counter_vec[0]/(float)count;
	p_attr->probability_vec[1] = 1 - p_attr->probability_vec[0];
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint DirectionEntropyFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
