/*
 * PacketLengthPairsReoccurring.h
 *
 *  Created on: 18.11.2014
 *      Author: adam
 */

#ifndef PACKETLENGTHPAIRSREOCCURRING_H_
#define PACKETLENGTHPAIRSREOCCURRING_H_

#include "Feature.h"
#include <iostream>

class PacketLengthPairsReoccurring: public Feature {
private:
	const uint THRESH = 8;
public:
	PacketLengthPairsReoccurring(uint id);
	virtual ~PacketLengthPairsReoccurring();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* PACKETLENGTHPAIRSREOCCURRING_H_ */
