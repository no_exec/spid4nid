/*
 * ActionReactionFirst3ByteHashFeature.cpp
 *
 *  Created on: 9.11.2014
 *      Author: adam
 */

#include "ActionReactionFirst3ByteHashFeature.h"

#define VEC_LEN 256

using std::vector;

ActionReactionFirst3ByteHashFeature::ActionReactionFirst3ByteHashFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
//	THRESH = 4;
}

ActionReactionFirst3ByteHashFeature::~ActionReactionFirst3ByteHashFeature() {
	// TODO Auto-generated destructor stub
}

uint ActionReactionFirst3ByteHashFeature::updateFeature(Flow* p_flow) {
	bool first = false;
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
		first = true;
	}
	if (p_flow->m_packet_num > THRESH) return SUCCESS;
	uint retcode = (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	if (first || p_flow->m_last_dir != packet.direction) {
		if (!packet.length) return retcode;
		int num_bytes = packet.length < 3 ? packet.length : 3;
		uint cross_sum = 0;
		for (int j = 0; j < num_bytes; j++) {
			uchar byte = packet.data[j],
				  rest = 0;
			do {
				rest = byte % 10;
				byte = byte / 10;
				cross_sum += rest;
			} while (byte > 0);
		}
		p_attr->counter_vec[cross_sum % VEC_LEN]++;
		for (uint i = 0; i < VEC_LEN; i++) {
			p_attr->probability_vec[i] = (float)p_attr->counter_vec[i]/(float)(p_flow->m_packet_num);
		}
	}

	return retcode;
}

attr_fingerprint ActionReactionFirst3ByteHashFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
