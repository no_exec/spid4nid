/*
 * FirstNonEmptyPayloadSize.cpp
 *
 *  Created on: 1.8.2014
 *      Author: adam
 */

#include "FirstNonEmptyPayloadSize.h"

#define VEC_LEN 2

using std::vector;

FirstNonEmptyPayloadSize::FirstNonEmptyPayloadSize(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub

}

FirstNonEmptyPayloadSize::~FirstNonEmptyPayloadSize() {
	// TODO Auto-generated destructor stub
}

uint FirstNonEmptyPayloadSize::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	if (p_attr->counter_vec[0] || p_attr->counter_vec[1]) {
		return SUCCESS;
	}
	if (p_attr->counter_vec[0] != 0 || packet.length == 0) return SUCCESS;
	p_attr->counter_vec[0] = packet.length;
	p_attr->counter_vec[1] = 1500 - p_attr->counter_vec[0];
	p_attr->probability_vec[0] = (float)p_attr->counter_vec[0]/(float)1500;
	p_attr->probability_vec[1] = 1 - p_attr->probability_vec[0];
	return SUCCESS;
}
attr_fingerprint FirstNonEmptyPayloadSize::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
