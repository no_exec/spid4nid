/*
 * TwoByteHashFeature.h
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#ifndef TWOBYTEHASHFEATURE_H_
#define TWOBYTEHASHFEATURE_H_

#include "Feature.h"

class TwoByteHashFeature: public Feature {
private:
	const uint THRESH = 16;
public:
	TwoByteHashFeature(uint id);
	virtual ~TwoByteHashFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* TWOBYTEHASHFEATURE_H_ */
