/*
 * ThreeByteHashFeature.h
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#ifndef THREEBYTEHASHFEATURE_H_
#define THREEBYTEHASHFEATURE_H_

#include "Feature.h"

class ThreeByteHashFeature: public Feature {
private:
	const uint THRESH = 16;
public:
	ThreeByteHashFeature(uint id);
	virtual ~ThreeByteHashFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* THREEBYTEHASHFEATURE_H_ */
