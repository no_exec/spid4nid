/*
 * FirstNonEmptyPayloadSize.h
 *
 *  Created on: 1.8.2014
 *      Author: adam
 */

#ifndef FIRSTNONEMPTYPAYLOADSIZE_H_
#define FIRSTNONEMPTYPAYLOADSIZE_H_

#include "Feature.h"

class FirstNonEmptyPayloadSize: public Feature {
public:
	FirstNonEmptyPayloadSize(uint id);
	virtual ~FirstNonEmptyPayloadSize();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* FIRSTNONEMPTYPAYLOADSIZE_H_ */
