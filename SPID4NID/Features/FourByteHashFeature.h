/*
 * FourByteHashFeature.h
 *
 *  Created on: 15.11.2015
 *      Author: adam
 */

#ifndef FOURBYTEHASHFEATURE_H_
#define FOURBYTEHASHFEATURE_H_

#include "Feature.h"

class FourByteHashFeature : public Feature {
private:
	const uint THRESH = 8;
public:
	FourByteHashFeature(uint id);
	virtual ~FourByteHashFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* FOURBYTEHASHFEATURE_H_ */
