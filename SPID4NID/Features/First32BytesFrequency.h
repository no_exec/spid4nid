/*
 * First32BytesFrequency.h
 *
 *  Created on: 2.8.2014
 *      Author: adam
 */

#ifndef FIRST32BYTESFREQUENCY_H_
#define FIRST32BYTESFREQUENCY_H_

#include "Feature.h"

class First32BytesFrequency: public Feature {
private:
	const uint THRESH = 16;
public:
	First32BytesFrequency(uint id);
	virtual ~First32BytesFrequency();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* FIRST32BYTESFREQUENCY_H_ */
