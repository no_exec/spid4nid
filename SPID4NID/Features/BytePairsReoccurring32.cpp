/*
 * BytePairsReoccurring32.cpp
 *
 *  Created on: 2.8.2014
 *      Author: adam
 */

#include <vector>
#include <numeric>
#include "BytePairsReoccurring32.h"

#define VEC_LEN 256

using std::vector;

BytePairsReoccurring32::BytePairsReoccurring32(uint id) :
		Feature(id) {
	// TODO Auto-generated constructor stub

}

BytePairsReoccurring32::~BytePairsReoccurring32() {
	// TODO Auto-generated destructor stub
}

uint BytePairsReoccurring32::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH) return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	int len = (32 <= packet.length) ? 32 : packet.length;
	if (!len)
		return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
	for (int j = 0; j < len - 1; j++) {
		if (packet.data[j] == packet.data[j + 1]) {
			p_attr->counter_vec[packet.data[j]]++;
		}
	}
	if (p_flow->m_data_sizes == 0)
		return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
	int c = 0;
	for (int i = 0; i < VEC_LEN; i++) {
		c += p_attr->counter_vec[i];
	}
	for (int i = 0; i < VEC_LEN; i++) {
		if (p_attr->counter_vec[i]) {
			p_attr->probability_vec[i] = (float) p_attr->counter_vec[i]
					/ (float) c;
		}
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}
attr_fingerprint BytePairsReoccurring32::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
