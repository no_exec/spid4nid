/*
 * DirectionChangesFeature.h
 *
 *  Created on: 31.7.2014
 *      Author: adam
 */

#ifndef DIRECTIONCHANGESFEATURE_H_
#define DIRECTIONCHANGESFEATURE_H_

#include "Feature.h"

class DirectionChangesFeature: public Feature {
private:
	const uint THRESH = 16;
public:
	DirectionChangesFeature(uint id);
	virtual ~DirectionChangesFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* DIRECTIONCHANGESFEATURE_H_ */
