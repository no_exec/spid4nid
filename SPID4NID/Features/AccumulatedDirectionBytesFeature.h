/*
 * AccumulatedDirectionBytesFeature.h
 *
 *  Created on: 9.4.2014
 *      Author: adam
 */

#ifndef ACCUMULATEDDIRECTIONBYTESFEATURE_H_
#define ACCUMULATEDDIRECTIONBYTESFEATURE_H_

#include "Feature.h"

class AccumulatedDirectionBytesFeature : public Feature {
private:
	const uint THRESH = 8;
public:
	AccumulatedDirectionBytesFeature(uint id);
	virtual ~AccumulatedDirectionBytesFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* ACCUMULATEDDIRECTIONBYTESFEATURE_H_ */
