/*
 * DirectionByteFrequencyFeature.cpp
 *
 *  Created on: 9.4.2014
 *      Author: adam
 */

#include <vector>
#include "DirectionByteFrequencyFeature.h"

#define VEC_LEN 512

using std::vector;

DirectionByteFrequencyFeature::DirectionByteFrequencyFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 128;
}

DirectionByteFrequencyFeature::~DirectionByteFrequencyFeature() {
	// TODO Auto-generated destructor stub
}

uint DirectionByteFrequencyFeature::updateFeature(Flow* p_flow) {
	packet_info packet = p_flow->packet_buffer.front();
	uchar* packet_data = packet.data;
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_data_sizes - packet.length > THRESH)
		return SUCCESS;
#endif
	if (packet.length == 0) return (p_flow->m_data_sizes < THRESH) ? HUNGRY : SUCCESS;
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	uint offset = packet.direction ? 0 : VEC_LEN/2;
	for (uint i = 0; i < packet.length; i++) {
		p_attr->counter_vec[offset + packet_data[i]]++;
	}
	for (int i = 0; i < VEC_LEN; i++) {
		p_attr->probability_vec[i] = (float)p_attr->counter_vec[i]/(float)p_flow->m_data_sizes;
	}
	return (p_flow->m_data_sizes < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint DirectionByteFrequencyFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
