/*
 * FourByteHashFeature.cpp
 *
 *  Created on: 15.11.2015
 *      Author: adam
 */

#include "FourByteHashFeature.h"

#define VEC_LEN 256

using std::vector;

FourByteHashFeature::FourByteHashFeature(uint id) : Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 8;
}

FourByteHashFeature::~FourByteHashFeature() {
	// TODO Auto-generated destructor stub
}

uint FourByteHashFeature::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
	//TODO continue to calculate when SUCCESS?
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH)
		return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	if (packet.length < 4) return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
	for (uint i = 0; i < packet.length - 3; i++) {
		p_attr->counter_vec[packet.data[i] ^ packet.data[i+1] ^ packet.data[i+2] ^ packet.data[i+3]]++;
	}
	int count = 0;
	for (int i = 0; i < VEC_LEN; i++) {
		count += p_attr->counter_vec[i];
	}
	for (int i = 0; i < VEC_LEN; i++) {
		p_attr->probability_vec[i] = p_attr->counter_vec[i]/(float)count;
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint FourByteHashFeature::mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
