/*
 * BytePairsReoccurring32.h
 *
 *  Created on: 2.8.2014
 *      Author: adam
 */

#ifndef BYTEPAIRSREOCCURRING32_H_
#define BYTEPAIRSREOCCURRING32_H_

#include "Feature.h"

#define SUCCESS 0

class BytePairsReoccurring32: public Feature {
private:
	const uint THRESH = 16;
public:
	BytePairsReoccurring32(uint id);
	virtual ~BytePairsReoccurring32();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* BYTEPAIRSREOCCURRING32_H_ */
