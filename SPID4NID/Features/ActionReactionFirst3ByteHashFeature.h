/*
 * ActionReactionFirst3ByteHashFeature.h
 *
 *  Created on: 9.11.2014
 *      Author: adam
 */

#ifndef ACTIONREACTIONFIRST3BYTEHASHFEATURE_H_
#define ACTIONREACTIONFIRST3BYTEHASHFEATURE_H_

#include "Feature.h"

class ActionReactionFirst3ByteHashFeature : public Feature {
private:
	const uint THRESH = 4;
public:
	ActionReactionFirst3ByteHashFeature(uint id);
	virtual ~ActionReactionFirst3ByteHashFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* ACTIONREACTIONFIRST3BYTEHASHFEATURE_H_ */
