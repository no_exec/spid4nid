/*
 * NewLineEqualityFeature.h
 *
 *  Created on: 10.4.2014
 *      Author: adam
 */

#ifndef NEWLINEEQUALITYFEATURE_H_
#define NEWLINEEQUALITYFEATURE_H_

#include "Feature.h"

class NewLineEqualityFeature : public Feature {
private:
	const uint THRESH = 8;
public:
	NewLineEqualityFeature(uint id);
	virtual ~NewLineEqualityFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* NEWLINEEQUALITYFEATURE_H_ */
