/*
 * ControlCharacterRatio.h
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#ifndef CONTROLCHARACTERRATIO_H_
#define CONTROLCHARACTERRATIO_H_

#include "Feature.h"

class ControlCharacterRatio: public Feature {
private:
	const uint THRESH = 16;
public:
	ControlCharacterRatio(uint id);
	virtual ~ControlCharacterRatio();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* CONTROLCHARACTERRATIO_H_ */
