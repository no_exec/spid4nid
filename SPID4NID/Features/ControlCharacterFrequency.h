/*
 * ControlCharacterFrequency.h
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#ifndef CONTROLCHARACTERFREQUENCY_H_
#define CONTROLCHARACTERFREQUENCY_H_

#include "Feature.h"

class ControlCharacterFrequency: public Feature {
private:
	const uint THRESH = 32;
public:
	ControlCharacterFrequency(uint id);
	virtual ~ControlCharacterFrequency();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* CONTROLCHARACTERFREQUENCY_H_ */
