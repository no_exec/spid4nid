/*
 * Entropy.h
 *
 *  Created on: 10.11.2014
 *      Author: adam
 */

#ifndef ENTROPY_H_
#define ENTROPY_H_

#include "Feature.h"

class EntropyFeature : public Feature {
private:
	const uint THRESH = 128;
public:
	EntropyFeature(uint id);
	virtual ~EntropyFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* ENTROPY_H_ */
