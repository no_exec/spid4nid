/*
 * First4BytesCrossSum.h
 *
 *  Created on: 2.8.2014
 *      Author: adam
 */

#ifndef FIRST4BYTESCROSSSUM_H_
#define FIRST4BYTESCROSSSUM_H_

#include "Feature.h"

class First4BytesCrossSum: public Feature {
private:
	const uint THRESH = 2;
public:
	First4BytesCrossSum(uint id);
	virtual ~First4BytesCrossSum();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* FIRST4BYTESCROSSSUM_H_ */
