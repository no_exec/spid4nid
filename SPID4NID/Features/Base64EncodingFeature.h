/*
 * Base64EncodingFeature.h
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#ifndef BASE64ENCODINGFEATURE_H_
#define BASE64ENCODINGFEATURE_H_

#include "Feature.h"

class Base64EncodingFeature: public Feature {
private:
	const uint THRESH = 32;
public:
	Base64EncodingFeature(uint id);
	virtual ~Base64EncodingFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* BASE64ENCODINGFEATURE_H_ */
