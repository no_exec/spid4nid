/*
 * PayloadSizesFeature.cpp
 *
 *  Created on: 15.11.2015
 *      Author: adam
 */

#include "PayloadSizesFeature.h"

#define VEC_LEN 256

using std::vector;

PayloadSizesFeature::PayloadSizesFeature(uint id) :
		Feature(id) {
	// TODO Auto-generated constructor stub
	//THRESH = 16;
}

PayloadSizesFeature::~PayloadSizesFeature() {
	// TODO Auto-generated destructor stub
}

uint PayloadSizesFeature::updateFeature(Flow* p_flow) {
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(VEC_LEN, 0);
		attr.probability_vec = vector<float>(VEC_LEN, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_packet_num > THRESH) return SUCCESS;
#endif
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	packet_info packet = p_flow->packet_buffer.front();
	if (!packet.length) {
		return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
	}
	p_attr->counter_vec[(p_flow->m_packet_num - 1) % VEC_LEN] = packet.length;
	for (int i = 0; i < VEC_LEN; i++) {
		p_attr->probability_vec[i] = p_attr->counter_vec[i] / (float) p_flow->m_data_sizes;
	}
	return (p_flow->m_packet_num < THRESH) ? HUNGRY : SUCCESS;
}

attr_fingerprint PayloadSizesFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
