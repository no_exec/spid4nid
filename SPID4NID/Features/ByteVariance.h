/*
 * ByteVariance.h
 *
 *  Created on: 17.11.2014
 *      Author: adam
 */

#ifndef BYTEVARIANCE_H_
#define BYTEVARIANCE_H_

#include "Feature.h"

class ByteVariance: public Feature {
private:
	const uint THRESH = 96;
public:
	ByteVariance(uint id);
	virtual ~ByteVariance();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* BYTEVARIANCE_H_ */
