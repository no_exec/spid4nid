/*
 * DirectionByteCountFeature.h
 *
 *  Created on: 9.4.2014
 *      Author: adam
 */

#ifndef DIRECTIONBYTECOUNTFEATURE_H_
#define DIRECTIONBYTECOUNTFEATURE_H_

#include "Feature.h"

class DirectionByteCountFeature : public Feature {
private:
	const uint THRESH = 64;
public:
	DirectionByteCountFeature(uint id);
	virtual ~DirectionByteCountFeature();

	uint updateFeature(Flow* p_flow);
	attr_fingerprint mergeFeatureVectors(
			attr_fingerprint fpr1, attr_fingerprint fpr2);
};

#endif /* DIRECTIONBYTECOUNTFEATURE_H_ */
