/*
 * ByteFrequencyFeature.cpp
 *
 *  Created on: 22.3.2014
 *      Author: adam
 */

#include <vector>
#include "ByteFrequencyFeature.h"

using std::vector;

#define CHARS_NUM 256

ByteFrequencyFeature::ByteFrequencyFeature(uint id): Feature(id) {
	// TODO Auto-generated constructor stub

}

ByteFrequencyFeature::~ByteFrequencyFeature() {
	// TODO Auto-generated destructor stub
}

uint ByteFrequencyFeature::updateFeature(Flow* p_flow) {
	packet_info packet = p_flow->packet_buffer.front();
	uchar* packet_data = packet.data;
	if (p_flow->fingerprints.find(m_id) == p_flow->fingerprints.end()) {
		attr_fingerprint attr;
		attr.counter_vec = vector<uint>(CHARS_NUM, 0);
		attr.probability_vec = vector<float>(CHARS_NUM, 0.0f);
		p_flow->fingerprints[m_id] = attr;
	}
#ifdef REPLETE
	if (p_flow->m_data_sizes - packet.length > THRESH) return SUCCESS;
#endif
	if (packet.length == 0) return (p_flow->m_data_sizes < THRESH) ? HUNGRY : SUCCESS;
	attr_fingerprint* p_attr = &(p_flow->fingerprints[m_id]);
	for (uint i = 0; i < packet.length; i++) {
		p_attr->counter_vec[packet_data[i]]++;
	}
	for (int i = 0; i < CHARS_NUM; i++) {
		p_attr->probability_vec[i] = (float)(p_attr->counter_vec[i])/(float)p_flow->m_data_sizes;
	}
	return (p_flow->m_data_sizes < 96) ? HUNGRY : SUCCESS;
}

attr_fingerprint ByteFrequencyFeature::mergeFeatureVectors(
		attr_fingerprint fpr1, attr_fingerprint fpr2) {
	attr_fingerprint attr;
	return attr; // currently not implemented
}
