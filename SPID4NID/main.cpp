/*
 * main.cpp
 *
 *  Created on: 23.3.2014
 *      Author: adam
 */

#include "ProgramManager.h"

#include <editline/readline.h>

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <iostream>

using namespace std;

char* commands[] = { "train", "validate", "classify", "test", "save", "load",
		"help", "--valdir", "--log", "--state", "--mode", (char*) NULL };

char* progname;
void* xmalloc(size_t size) {
	register void *value = (void*) malloc(size);
	if (value == 0)
		fprintf(stderr, "virtual memory exhausted");
	return value;
}

void initialize_readline();
int execute_line(char *line);

char* dupstr(char* s) {
	char *r;

	r = (char*) xmalloc(strlen(s) + 1);
	strcpy(r, s);
	return (r);
}

char *
stripwhite(char* str) {
	register char *s, *t;

	for (s = str; isspace(*s); s++)
		;

	if (*s == 0)
		return (s);

	t = s + strlen(s) - 1;
	while (t > s && isspace(*t))
		t--;
	*++t = '\0';

	return s;
}

ProgramManager* progMng = NULL;

int main(int argc __attribute__((__unused__)), char **argv) {
	progMng = new ProgramManager();
	initialize_readline();
	stifle_history(10);
	progname = argv[0];

	char* line = NULL;
	char* s = NULL;
	while (line = readline(">")) {
		if (!line) break;
		s = stripwhite(line);
		if (*s) {
			char* expansion;
			int result = 0;
			result = history_expand(s, &expansion);
			if (result < 0 || result == 2) {
				fprintf(stderr, "%s\n", expansion);
			} else {
				add_history(expansion);
				execute_line(expansion);
			}
			free(expansion);
		}
		free(line);
	}
	exit(0);
//	progMng->Main();
//	ProtocolManager proto_mng;
//	proto_mng.train(string("pcaps"),0,string("valdir"));
//	proto_mng.classify(string("valdir/dark_comet.pcap"),0);
//	proto_mng.classify(string("valdir/xtreme.pcap"),0);
	//proto_mng.classify(string("tests/piece_of_dark.pcap"),0);
	//proto_mng.classify(string("tests/piece_of_extreme.pcap"),0);
	//proto_mng.classify(string("tests/xtreme_piece.pcap"),0);
	//proto_mng.classify(string("tests/smtps.pcap"),0);
	//proto_mng.classify(string("tests/dark.pcap"),0);
	//proto_mng.classify(string("tests/dark_comet2.pcap"),0);
	return 0;
}

int execute_line(char* buffer) {
	uint func = 6;
	bool first = true;
	std::string train_dir = "", val_dir = "", class_dir = "", test_dir = "",
			output_log = "", output_state = "", save_file = "", load_file = "";
	uint mode = 0;
	char* token;
	token = strtok(buffer, " ");
	while (token != NULL) {
		if (first && !strncmp(buffer, "train", 5)) {
			token = strtok(NULL, " ");
			train_dir = string(token);
			func = 0;
		} else if (first && !strncmp(buffer, "validate", 8)) {
			token = strtok(NULL, " ");
			val_dir = string(token);
			func = 1;
		} else if (first && !strncmp(buffer, "classify", 8)) {
			token = strtok(NULL, " ");
			class_dir = string(token);
			func = 2;
		} else if (first && !strncmp(buffer, "test", 4)) {
			token = strtok(NULL, " ");
			test_dir = string(token);
			func = 3;
		} else if (first && !strncmp(buffer, "save", 4)) {
			token = strtok(NULL, " ");
			save_file = string(token);
			func = 4;
		} else if (first && !strncmp(buffer, "load", 4)) {
			token = strtok(NULL, " ");
			load_file = string(token);
			func = 5;
		} else if (first && !strncmp(buffer, "help", 4)) {
			func = 6;
		} else if (!first && !strncmp(token, "--valdir", 8)) {
			token = strtok(NULL, " ");
			val_dir = string(token);
		} else if (!first && !strncmp(token, "--log", 5)) {
			token = strtok(NULL, " ");
			output_log = string(token);
		} else if (!first && !strncmp(token, "--state", 7)) {
			token = strtok(NULL, " ");
			output_state = string(token);
		} else if (!first && !strncmp(token, "--mode", 6)) {
			token = strtok(NULL, " ");
			mode = atoi(token);
		}
		first = false;
		token = strtok(NULL, " ");
	}
	switch (func) {
	case 0:
		progMng->train(train_dir,val_dir,output_log,output_state,mode);
		break;
	case 1:
		progMng->optimize(val_dir,output_state,output_log,mode);
		break;
	case 2:
		progMng->classify(class_dir,output_log,0);
		break;
	case 3:
		progMng->test(test_dir,output_log,0);
		break;
	case 4:
		progMng->storeState(save_file,mode);
		break;
	case 5:
		progMng->loadState(load_file);
		break;
	case 6:
		cout << "Supported commands:" << std::endl;
		cout << "train train_dir [--val_dir val_dir][--log output_log] [--state output_state] [--mode mode]" << std::endl;
		cout << "validate val_dir [--state output_state] [--log output_log] [--mode mode]" << std::endl;
		cout << "classify class_dir [--log output_log]" << std::endl;
		cout << "test test_dir [--log output_log]" << std::endl;
		cout << "save save_file [--mode mode]" << std::endl;
		cout << "load load_file" << std::endl;
		cout << "help" << std::endl;
		cout << "For exhaustive explanation, see attached README.txt file" << std::endl;
		break;
	default:
		std::cout << "Operation was not specified." << std::endl;
	}
	return 0;
}

char *command_generator(const char *, int);
char **dip_completion(const char *, int, int);

void
initialize_readline ()
{
   /* Allow conditional parsing of the ~/.inputrc file. */
   rl_readline_name = "dip";

   /* Tell the completer that we want a crack first. */
   rl_attempted_completion_function = dip_completion;
}

char **
dip_completion (const char* text, int start, int end __attribute__((__unused__)))
{
   char **matches;

   matches = (char **)NULL;

matches = completion_matches (text, command_generator);

  return (matches);
}
char *
command_generator (const char* text, int state)
{
   static int list_index, len;
   char *name;

   if (!state)
   {
      list_index = 0;
      len = strlen (text);
   }

   while ((name = commands[list_index]))
   {
      list_index++;

      if (strncmp (name, text, len) == 0)
         return (dupstr(name));
   }

   return ((char *)NULL);
}
