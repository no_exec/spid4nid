/*
 * ProtocolManager.h
 *
 *  Created on: 18.3.2014
 *      Author: adam
 */

#ifndef PROTOCOLMANAGER_H_
#define PROTOCOLMANAGER_H_

#include <unordered_map>
#include <tuple>
#include <fstream>
#include <iostream>
#include "definitions.h"
#include "FlowId.h"
#include "Flow.h"
#include "Classifier.h"
#include "Optimizer.h"

#define SUCCESS 0
#define WRONG_NAME 1
#define NONEX_DIR 2

#define WRONG_L3_PROTO 3
#define WRONG_L4_PROTO 4

#define OPTIMIZE 1

const int ETH_FRAME_HDR_SIZE = 14;
const int IP_ADDR_SIZE = 4;
const uchar TCP = 0x06;
const uchar UDP = 0x11;

class ProtocolManager {
private:
	Classifier* m_classifier;
	std::unordered_map<FlowId,Flow*,FlowIdHasher> flows;
	float m_score;

	uint pcapExtractInfo(const uchar* pcap_packet, FlowId& flow, packet_info& packet);
	Flow* lookupFlow(FlowId flow_id, packet_info& packet);

public:
	ProtocolManager();
	virtual ~ProtocolManager();
	static bool triple_compare(std::tuple<std::string,uint,float> a, std::tuple<std::string,uint,float> b);
	uint train(std::string directory, std::string val_dir = "", std::ostream& log = std::cout);
	int classify(std::string file, test_results& results, std::ostream& log = std::cout, uint mode = 0);
	uint validate(std::string directory, std::ostream& log = std::cout, uint mode = 0);
	Classifier* getClassifier();
};



#endif /* PROTOCOLMANAGER_H_ */
