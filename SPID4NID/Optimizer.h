/*
 * Optimizer.h
 *
 *  Created on: 13.9.2014
 *      Author: adam
 */

#ifndef OPTIMIZER_H_
#define OPTIMIZER_H_

#include "definitions.h"
#include <iostream>

class Optimizer {
public:
	Optimizer();
	virtual ~Optimizer();

	virtual uint optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, std::ostream& log = std::cout, uint mode = 0) = 0;
};

#endif /* OPTIMIZER_H_ */
