/*
 * ProtocolManager.cpp
 *
 *  Created on: 18.3.2014
 *      Author: adam
 */

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <sys/types.h>
#include <dirent.h>
#include <algorithm>
#include "pcap.h"
#include "ProtocolManager.h"
#include "Classifier.h"
#include "Flow.h"
#if defined(EX2D)
#include "Exhaustive2DOptimizer.h"
#elif defined (GA1D)
#include "GA1DBinaryOptimizer.h"
#elif defined (EX1D)
#include "ExhaustiveOptimizer.h"
#else
#include "GA2DBinaryOptimizer.h"
#endif

using std::string;
using std::cout;
using std::endl;
using std::ostringstream;
using std::ostream;

#define FIN 1
#define SYN 2
#define RST 4
#define ACK 16

ProtocolManager::ProtocolManager(): m_score(0) {
	// TODO Auto-generated constructor stub
	m_classifier = new Classifier();
}

ProtocolManager::~ProtocolManager() {
	// TODO Auto-generated destructor stub
	delete m_classifier;
	flows.clear();
}

Flow* ProtocolManager::lookupFlow(FlowId flow_id, packet_info& packet) {
	std::unordered_map<FlowId,Flow*,FlowIdHasher>::iterator pos;
	Flow* p_flow;
	if ((pos = flows.find(flow_id)) == flows.end()) {
		if (flow_id.m_protocol == TCP && !(packet.flags & SYN)) return nullptr;
		p_flow = new Flow(flows.size(),0,0,false);
		packet.order = 1;
		p_flow->packet_buffer.push_front(packet);
		if (flow_id.m_protocol == UDP) {
			p_flow->m_packet_num = 1;
			p_flow->m_state = Flow::active;
		}
		flows[flow_id] = p_flow;
	} else {
		FlowId flow_id_orig = pos->first;
		p_flow = pos->second;
		packet_info prev_packet = p_flow->packet_buffer.front();
		if (flow_id_orig.m_protocol == UDP) {
			packet.order = prev_packet.order + 1;
		} else {
			if ((packet.flags & FIN) || (packet.flags & RST)) {
				p_flow->m_state = Flow::inactive;
				return nullptr;
			}
			if (p_flow->m_state == Flow::S) {
				if ((packet.flags & SYN) && (packet.flags & ACK)) {
					if (prev_packet.seq + 1 != packet.ack) return nullptr;
					p_flow->m_state = Flow::SSA;
					packet.order = prev_packet.order + 1;
				} else return nullptr;
			} else if (p_flow->m_state == Flow::SSA) {
				if (packet.flags & ACK) {
					if (prev_packet.seq + 1 != packet.ack || prev_packet.ack != packet.seq)
						return nullptr;
					p_flow->m_state = Flow::SSAA;
					packet.order = prev_packet.order + 1;
				} else return nullptr;
			} else if (p_flow->m_state == Flow::SSAA) {
				p_flow->m_state = Flow::active;
				p_flow->packet_buffer.clear();
				p_flow->m_packet_num = 0;
				packet.order = 1;
			} else if (p_flow->m_state == Flow::inactive) {
				if (packet.flags & SYN) p_flow->m_state = Flow::S; //is feature updating correct?
				else return nullptr;
			}
		}

		if (p_flow->m_state == Flow::active) {
			if (flow_id.m_src_ip != flow_id_orig.m_src_ip) {
				p_flow->m_biflow = true;
				packet.direction = false; //true for src to dst, false for dst to src
			}
			p_flow->m_packet_num++;
			if((p_flow->m_packet_num != 1) && (prev_packet.direction != packet.direction)) {
				p_flow->m_dir_changes++;
			}
		}
		p_flow->packet_buffer.push_front(packet);
	}
	return p_flow;
}

uint ProtocolManager::train(string directory, string val_dir /*= ""*/, ostream& log) {
	struct dirent** dp;
	if (directory != "") {
		int n = scandir(directory.c_str(), &dp, 0, alphasort);
		if (n < 0) return NONEX_DIR;
		for (int i = 0; i < n; i++) {
			if (!strcmp(dp[i]->d_name,".") || !strcmp(dp[i]->d_name,"..")) continue;
			string filename = directory + "/" + string(dp[i]->d_name);
			string proto_name = string(dp[i]->d_name).substr(0,string(dp[i]->d_name).find(".pcap"));
			log << filename << " is being read" << endl;
			char errbuff[PCAP_ERRBUF_SIZE];
			pcap_t* pcap = pcap_open_offline(filename.c_str(), errbuff);
			struct pcap_pkthdr* header;
			const u_char* data;
			int retval;
			Flow* p_flow;
			while ((retval = pcap_next_ex(pcap, &header, &data)) >= 0) {
				FlowId flow_id;
				packet_info packet;
				pcapExtractInfo(data, flow_id, packet);
				p_flow = lookupFlow(flow_id,packet);
				if ((p_flow != nullptr) && (p_flow->m_state == Flow::active)) {
					p_flow->m_data_sizes += packet.length;
					m_classifier->train(p_flow, proto_name, log, 0);
					p_flow->m_last_dir = packet.direction;
					p_flow->m_last_size = packet.length;
				}
			}
			std::unordered_map<FlowId,Flow*,FlowIdHasher>::iterator iter;
			for (iter = flows.begin(); iter != flows.end(); iter++) {
				if (!iter->second->fingerprints.empty())
					m_classifier->train(iter->second,proto_name, log, FINISH);
			}
			flows.clear();
			free(dp[i]);
		}
		free(dp);
	}
	if (val_dir != "") return validate(val_dir, log);
	return SUCCESS;
}

uint ProtocolManager::validate(string val_dir, ostream& log, uint mode) {
	if (val_dir != "") {
		struct dirent** dp;
		int n = scandir(val_dir.c_str(), &dp, 0, alphasort);
		if (n < 0) return NONEX_DIR;
		test_dir_results overall_res;
		test_results res;
		for (int i = 0; i < n; i++) {
			if (!strcmp(dp[i]->d_name,".") || !strcmp(dp[i]->d_name,"..")) continue;
			string proto_name = string(dp[i]->d_name).substr(0,string(dp[i]->d_name).find(".pcap"));
			this->classify(val_dir + "/" + string(dp[i]->d_name), res, log, 0);
			std::pair<string,test_results> entry (proto_name,res);
			overall_res.push_back(entry);
		}
		Optimizer* opt;
#if defined(GA1D)
		opt = new GA1DBinaryOptimizer();
#elif defined(EX1D)
		opt = new ExhaustiveOptimizer();
#elif defined(EX2D)
		opt = new Exhaustive2DOptimizer();
#else
		opt = new GA2DBinaryOptimizer();
#endif
		configuration solution;
		opt->optimize(overall_res, &solution, m_classifier->numFeatures(), m_classifier->numProtocols(), log, 0);
		m_classifier->setConfiguration(solution);
		flows.clear();
	}
	return SUCCESS;
}

int ProtocolManager::classify(std::string file, test_results& results, ostream& log, uint mode) {
	log << file << " is being read" << endl;
	char errbuff[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_offline(file.c_str(), errbuff);
	struct pcap_pkthdr* header;
	const u_char* data;
	int retval;
	Flow* p_flow;
	classif_result result;
	configuration config = m_classifier->getConfiguration();
	if (!config.dim1 && !config.dim2) {
#if defined(GA1D) || defined(EX1D)
		config.width = m_classifier->numFeatures();
		config.dim1 = new bool[config.width];
		memset(config.dim1,true,config.width);
#else
		config.width = m_classifier->numFeatures();
		config.height = m_classifier->numProtocols();
		config.dim2 = new bool*[config.height];
		for (uint i = 0; i < config.height; i++) {
			config.dim2[i] = new bool[config.width];
			memset(config.dim2[i],true,config.width);
		}
		
#endif
		m_classifier->setConfiguration(config);
	}
	while ((retval = pcap_next_ex(pcap, &header, &data)) >= 0) {
		FlowId flow_id;
		packet_info packet;
		pcapExtractInfo(data, flow_id, packet);
		p_flow = lookupFlow(flow_id,packet);
		
		if ((p_flow != nullptr) && (p_flow->m_state == Flow::active)) {
			p_flow->m_data_sizes += packet.length;
			
			m_classifier->classify(p_flow,&result, log, 0);
			
			p_flow->m_last_dir = packet.direction;
			p_flow->m_last_size = packet.length;
		}
	}
	
	std::unordered_map<FlowId,Flow*,FlowIdHasher>::iterator flow_iter;
	for (flow_iter = flows.begin(); flow_iter != flows.end(); flow_iter++) {
		m_classifier->classify(flow_iter->second,&result, log, FINISH);
		std::sort(result.begin(), result.end(), triple_compare);
		results.push_back(result);
		classif_result::iterator iter;
	}
	flows.clear();
	return 0;
}

uint ProtocolManager::pcapExtractInfo(const uchar* pcap_data, FlowId& flow,
		packet_info& packet) {
	int pos = ETH_FRAME_HDR_SIZE - 2;//protocol
	char x, y;
	x = *(pcap_data + pos);
	y = *(pcap_data + pos + 1);
	if (x != 0x08 || y != 0x00) {
		return WRONG_L3_PROTO; //currently IPv4 only
	}
	pos += 2;
	char verihl = (char) *(pcap_data + pos);//version + header length

	int ihl = (verihl & 15)*4;
	pos += 2;
	unsigned char len1 = *(pcap_data + pos);
	unsigned char len2 = *(pcap_data + pos + 1);
	int len = (int)(len1<<8) + (int)len2;

	pos += 7;
	flow.m_protocol = *(pcap_data + pos);
	if (flow.m_protocol != TCP && flow.m_protocol != UDP) {
		return WRONG_L4_PROTO;//only TCP and UDP
	}
	pos += 3;
	unsigned char addr_src[IP_ADDR_SIZE];
	unsigned char addr_dst[IP_ADDR_SIZE];
	//IP addresses
	flow.m_src_ip = "";
	flow.m_dst_ip = "";
	for (int i = 0; i < IP_ADDR_SIZE; i++) {
		addr_src[i] = *(pcap_data + pos + i);
		addr_dst[i] = *(pcap_data + pos + i + IP_ADDR_SIZE);
		flow.m_src_ip += static_cast<ostringstream*>( &(ostringstream() << (int)addr_src[i]) )->str();
		flow.m_dst_ip += static_cast<ostringstream*>( &(ostringstream() << (int)addr_dst[i]) )->str();

		if (i < IP_ADDR_SIZE - 1) {
			flow.m_src_ip += ".";
			flow.m_dst_ip += ".";
		}
	}

	pos = ETH_FRAME_HDR_SIZE + ihl;
	int l4_proto_start_pos = pos;
	unsigned char port1 = *(pcap_data + pos);
	unsigned char port2 = *(pcap_data + pos + 1);
	flow.m_src_port = (int)(port1<<8) + (int)port2;
	port1 = *(pcap_data + pos + 2);
	port2 = *(pcap_data + pos + 3);
	flow.m_dst_port = (int)(port1<<8) + (int)port2;

	pos += 4;
	int l4_hlen = 0;
	packet.seq = 0;
	packet.ack = 0;
	if (flow.m_protocol == TCP) {
		uint temp = 0;
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 4; j++) {
				temp = *(pcap_data + pos);
				temp = temp << 8*(3-j);
				if (i == 0) packet.seq += temp;
				else packet.ack += temp;
				pos++;
			}
		}

		l4_hlen = 4*((pcap_data[pos] & 240)>>4);

		pos += 1;
		packet.flags = *(pcap_data + pos);
	} else {
		l4_hlen = 8; //UDP has fixed header length
		packet.flags = 0;
	}

	packet.direction = true;
	packet.length = len - ihl - l4_hlen;
	packet.data = new uchar[packet.length];
	packet.keep_stored = false;
	memcpy(packet.data, pcap_data + l4_proto_start_pos + l4_hlen, packet.length);
	return SUCCESS;
}

Classifier* ProtocolManager::getClassifier() {
	return m_classifier;
}

bool ProtocolManager::triple_compare(std::tuple<std::string,uint,float> a, std::tuple<std::string,uint,float> b) {
	return (std::get<2>(a) > std::get<2>(b));
}
