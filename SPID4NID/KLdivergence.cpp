/*
 * KLdivergence.cpp
 *
 *  Created on: 7.4.2014
 *      Author: adam
 */

#include <math.h>
#include <cstdio>
#include "KLdivergence.h"

KLdivergence::KLdivergence() {
	// TODO Auto-generated constructor stub

}

KLdivergence::~KLdivergence() {
	// TODO Auto-generated destructor stub
}

float KLdivergence::computeSimilarity(attr_fingerprint protocol,
		attr_fingerprint protocol_model) {
	uint len = protocol.probability_vec.size();
	if (len != protocol_model.probability_vec.size()) {
		fprintf(stderr, "Sizes of attribute fingerprints are incompatible!");
		exit(EXIT_FAILURE);
	}
	float sum = 0,
		  ratio = 1;
	for (uint i = 0; i < len; i++) {
		if (protocol_model.probability_vec[i] == 0 || protocol.probability_vec[i] == 0) continue;
		else ratio = protocol.probability_vec[i]/protocol_model.probability_vec[i];
		sum += log2(ratio) * protocol.probability_vec[i];
	}
	return sum;
}
