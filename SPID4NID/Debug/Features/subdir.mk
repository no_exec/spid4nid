

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Features/AccumulatedDirectionBytesFeature.cpp \
../Features/ActionReactionFirst3ByteHashFeature.cpp \
../Features/Base64EncodingFeature.cpp \
../Features/ByteBitValueFeature.cpp \
../Features/ByteFrequencyFeature.cpp \
../Features/BytePairsReoccurring32.cpp \
../Features/ByteVariance.cpp \
../Features/ControlCharacterFrequency.cpp \
../Features/ControlCharacterRatio.cpp \
../Features/DirectionByteCountFeature.cpp \
../Features/DirectionByteFrequencyFeature.cpp \
../Features/DirectionChangesFeature.cpp \
../Features/DirectionEntropyFeature.cpp \
../Features/EntropyFeature.cpp \
../Features/Feature.cpp \
../Features/First32BytesFrequency.cpp \
../Features/First4BytesCrossSum.cpp \
../Features/FirstBitPositionsFeature.cpp \
../Features/FirstNonEmptyPayloadSize.cpp \
../Features/FirstPacketPerDirectionNibblesFeature.cpp \
../Features/FourByteHashFeature.cpp \
../Features/NewLineEqualityFeature.cpp \
../Features/NibblePositionFrequencyFeature.cpp \
../Features/NullFrequencyFeature.cpp \
../Features/PacketLengthPairsReoccurring.cpp \
../Features/PayloadSizeChangesFeature.cpp \
../Features/PayloadSizeFrequency.cpp \
../Features/PayloadSizeHashPairs.cpp \
../Features/PayloadSizesFeature.cpp \
../Features/ThreeByteHashFeature.cpp \
../Features/TwoByteHashFeature.cpp 

OBJS += \
./Features/AccumulatedDirectionBytesFeature.o \
./Features/ActionReactionFirst3ByteHashFeature.o \
./Features/Base64EncodingFeature.o \
./Features/ByteBitValueFeature.o \
./Features/ByteFrequencyFeature.o \
./Features/BytePairsReoccurring32.o \
./Features/ByteVariance.o \
./Features/ControlCharacterFrequency.o \
./Features/ControlCharacterRatio.o \
./Features/DirectionByteCountFeature.o \
./Features/DirectionByteFrequencyFeature.o \
./Features/DirectionChangesFeature.o \
./Features/DirectionEntropyFeature.o \
./Features/EntropyFeature.o \
./Features/Feature.o \
./Features/First32BytesFrequency.o \
./Features/First4BytesCrossSum.o \
./Features/FirstBitPositionsFeature.o \
./Features/FirstNonEmptyPayloadSize.o \
./Features/FirstPacketPerDirectionNibblesFeature.o \
./Features/FourByteHashFeature.o \
./Features/NewLineEqualityFeature.o \
./Features/NibblePositionFrequencyFeature.o \
./Features/NullFrequencyFeature.o \
./Features/PacketLengthPairsReoccurring.o \
./Features/PayloadSizeChangesFeature.o \
./Features/PayloadSizeFrequency.o \
./Features/PayloadSizeHashPairs.o \
./Features/PayloadSizesFeature.o \
./Features/ThreeByteHashFeature.o \
./Features/TwoByteHashFeature.o 

CPP_DEPS += \
./Features/AccumulatedDirectionBytesFeature.d \
./Features/ActionReactionFirst3ByteHashFeature.d \
./Features/Base64EncodingFeature.d \
./Features/ByteBitValueFeature.d \
./Features/ByteFrequencyFeature.d \
./Features/BytePairsReoccurring32.d \
./Features/ByteVariance.d \
./Features/ControlCharacterFrequency.d \
./Features/ControlCharacterRatio.d \
./Features/DirectionByteCountFeature.d \
./Features/DirectionByteFrequencyFeature.d \
./Features/DirectionChangesFeature.d \
./Features/DirectionEntropyFeature.d \
./Features/EntropyFeature.d \
./Features/Feature.d \
./Features/First32BytesFrequency.d \
./Features/First4BytesCrossSum.d \
./Features/FirstBitPositionsFeature.d \
./Features/FirstNonEmptyPayloadSize.d \
./Features/FirstPacketPerDirectionNibblesFeature.d \
./Features/FourByteHashFeature.d \
./Features/NewLineEqualityFeature.d \
./Features/NibblePositionFrequencyFeature.d \
./Features/NullFrequencyFeature.d \
./Features/PacketLengthPairsReoccurring.d \
./Features/PayloadSizeChangesFeature.d \
./Features/PayloadSizeFrequency.d \
./Features/PayloadSizeHashPairs.d \
./Features/PayloadSizesFeature.d \
./Features/ThreeByteHashFeature.d \
./Features/TwoByteHashFeature.d 


# Each subdirectory must supply rules for building sources it contributes
Features/%.o: ../Features/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DREPLETE -O0 -g3 -Wall -std=c++11 -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


