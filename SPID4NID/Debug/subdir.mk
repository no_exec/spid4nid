

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Classifier.cpp \
../CosineSimilarity.cpp \
../Exhaustive2DOptimizer.cpp \
../ExhaustiveOptimizer.cpp \
../Flow.cpp \
../FlowId.cpp \
../GA1DBinaryOptimizer.cpp \
../GA2DBinaryOptimizer.cpp \
../KLdivergence.cpp \
../Metric.cpp \
../Optimizer.cpp \
../ProgramManager.cpp \
../ProtocolManager.cpp \
../XMLSerializer.cpp \
../main.cpp \
../tinyxml2.cpp 

OBJS += \
./Classifier.o \
./CosineSimilarity.o \
./Exhaustive2DOptimizer.o \
./ExhaustiveOptimizer.o \
./Flow.o \
./FlowId.o \
./GA1DBinaryOptimizer.o \
./GA2DBinaryOptimizer.o \
./KLdivergence.o \
./Metric.o \
./Optimizer.o \
./ProgramManager.o \
./ProtocolManager.o \
./XMLSerializer.o \
./main.o \
./tinyxml2.o 

CPP_DEPS += \
./Classifier.d \
./CosineSimilarity.d \
./Exhaustive2DOptimizer.d \
./ExhaustiveOptimizer.d \
./Flow.d \
./FlowId.d \
./GA1DBinaryOptimizer.d \
./GA2DBinaryOptimizer.d \
./KLdivergence.d \
./Metric.d \
./Optimizer.d \
./ProgramManager.d \
./ProtocolManager.d \
./XMLSerializer.d \
./main.d \
./tinyxml2.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DREPLETE -O0 -g3 -Wall -std=c++11 -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


