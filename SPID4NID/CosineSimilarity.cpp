/*
 * CosineSimilarity.cpp
 *
 *  Created on: 7.4.2014
 *      Author: adam
 */

#include "CosineSimilarity.h"
#include <math.h>
#include <cstdio>

CosineSimilarity::CosineSimilarity() {
	// TODO Auto-generated constructor stub

}

CosineSimilarity::~CosineSimilarity() {
	// TODO Auto-generated destructor stub
}

float CosineSimilarity::computeSimilarity(attr_fingerprint protocol,
		attr_fingerprint protocol_model) {
	uint len = protocol.probability_vec.size();
	if (len != protocol_model.probability_vec.size()) {
		fprintf(stderr, "Sizes of attribute fingerprints are incompatible!");
		exit(EXIT_FAILURE);
	}
	float nominator = 0, euclid_len1 = 0, euclid_len2 = 0, denominator = 1,
		  a = 0, b = 0;
	for (uint i = 0; i < len; i++) {
		a = protocol.probability_vec[i];
		b = protocol_model.probability_vec[i];
		nominator += a * b;
		euclid_len1 += a * a;
		euclid_len2 += b * b;
	}
	denominator = sqrt(euclid_len1) * sqrt(euclid_len2);
	if (denominator == 0) {
		if (sqrt(euclid_len1) == sqrt(euclid_len2)) {
			return 1.0;
		} else {
			return 0.0;
		}
	}
	return nominator/denominator;
}
