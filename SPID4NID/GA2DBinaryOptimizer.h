/*
 * GA2DBinaryOptimizer.h
 *
 *  Created on: 13.9.2014
 *      Author: adam
 */

#ifndef GA2DBINARYOPTIMIZER_H_
#define GA2DBINARYOPTIMIZER_H_

#include <ga/GA2DBinStrGenome.h>
#include <ga/GASimpleGA.h>
#include <ga/GASStateGA.h>
#include <iostream>
#include "Optimizer.h"

class GA2DBinaryOptimizer : public Optimizer {
public:
	static test_dir_results results;

	GA2DBinaryOptimizer();
	virtual ~GA2DBinaryOptimizer();

	uint optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, std::ostream& log = std::cout, uint mode = 0);
	static float Objective(GAGenome &);
	static int CustomCrossover(const GAGenome& p1, const GAGenome& p2, GAGenome* c1, GAGenome* c2);
};

#endif /* GA2DBINARYOPTIMIZER_H_ */
