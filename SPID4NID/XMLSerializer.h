/*
 * XMLSerializer.h
 *
 *  Created on: 3.2.2015
 *      Author: adam
 */

#ifndef XMLSERIALIZER_H_
#define XMLSERIALIZER_H_

#include "definitions.h"
#include "Classifier.h"
#include "tinyxml2.h"

using namespace tinyxml2;

class XMLSerializer {
public:
	XMLSerializer();
	virtual ~XMLSerializer();

	int storeState(Classifier* classifier, const char* out_file, uint mode);
	int loadState(Classifier* classifier, const char* in_file);

	int modelFingerprints(proto_fprints* fingerprints, XMLDocument* doc, XMLNode* root);
	int parseFingerprints(proto_fprints* fingerprints, XMLElement* root);
	int modelConfiguration(configuration config, XMLDocument* doc, XMLNode* root);
	int parseConfiguration(configuration* config, XMLElement* root);
};

#endif /* XMLSERIALIZER_H_ */
