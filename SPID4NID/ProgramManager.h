/*
 * ProgramManager.h
 *
 *  Created on: 3.2.2015
 *      Author: adam
 */

#ifndef PROGRAMMANAGER_H_
#define PROGRAMMANAGER_H_

#include "definitions.h"
#include "XMLSerializer.h"
#include "ProtocolManager.h"

#define PROTO_DB 1
#define CONFIG 2

class ProgramManager {
private:
	ProtocolManager* pmng;
	XMLSerializer* xml;
public:
	ProgramManager();
	virtual ~ProgramManager();

	//void Main();
	int storeState(std::string output_file, uint mode);
	int loadState(std::string input_file);
	int train(std::string train_dir, std::string val_dir = "", std::string output_log = "", std::string output_state = "", uint state_mode = PROTO_DB | CONFIG );
	int optimize(std::string val_dir, std::string output_state = "", std::string output_log = "", uint state_mode = PROTO_DB | CONFIG);
	int classify(std::string class_dir, std::string output_log = "", uint mode = 0);
	int test(std::string test_dir, std::string output_log = "", uint mode = 0);
};

#endif /* PROGRAMMANAGER_H_ */
