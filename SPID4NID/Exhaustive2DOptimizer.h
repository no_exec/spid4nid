/*
 * Exhaustive2DOptimizer.h
 *
 *  Created on: 1.10.2014
 *      Author: adam
 */

#ifndef EXHAUSTIVE2DOPTIMIZER_H_
#define EXHAUSTIVE2DOPTIMIZER_H_

#include "Optimizer.h"

class Exhaustive2DOptimizer : public Optimizer {
public:
	static test_dir_results results;

	Exhaustive2DOptimizer();
	virtual ~Exhaustive2DOptimizer();

	uint optimize(test_dir_results overall_results, configuration* solution, uint width, uint height, std::ostream& log = std::cout, uint mode = 0);
	static float Objective(bool* config, uint width, uint height, uint num);
	static float OverAllObjective(bool** config, uint width, uint height);
};

#endif /* EXHAUSTIVE2DOPTIMIZER_H_ */
