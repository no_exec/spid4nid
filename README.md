# spid4nid

A prototype of network IDS system based on optimized SPID approach for classification of malicious protocols

## Requirements
This program was tested on Linux only.
Before compilation of this program, you should have installed
following packages:

gcc-c++
m4
flex
bison
libpcap-devel
galib
ncurses-devel
libedit-devel

All above packages should be found in standard repositories in most Linux distributions
with the exception of galib (version 2.4.7 was used) which can be found at:
http://lancet.mit.edu/ga/dist/

## Compilation
To compile the program, please do:

```
$ cd SPID4NID/Debug
$ make all
```

After this, SPID4NID binary will be created in Debug directory.

## Compilation options
By default, classifier uses GA2D optimization,
you can choose also GA1D, EX1D, EX2D (exhaustive search).

For example add -DGA1D into subdir.mk files (in Debug and Debug/Features).

-DREPLETE is switched on by default, attribute fingerprints are updated
until the certain amount of data/packets/direction changes, etc. has been reached.

## Usage
Following commands are supported:

```
train train_dir [--val_dir val_dir][--log output_log] [--state output_state]
[--mode mode]
```

Used for training the protocols. It takes one positional argument
train_dir-path to directory with pcap files. The names of these
files will be used for the names of protocols.

val_dir is optional argument and is expected to be the path to
the directory with pcap files of corresponding protocols that are
used for optimization. Note that the corresponding files must
have the same name as the files in train_dir. If no val_dir is
given, optimization will not happen.

output_log is optional argument used for storing the results of
training and optimization into a simple text format. If no out-
put_log is given, output will be directed to the standard output.
output_state is optional argument containing the path to XML
file that is to be newly created. It is used for storing the database
of fingerprints that will be created after training or storing the
generated configuration after training and optimizing. This can
be used for later loading the fingerprints without the need for
training, loading the configuration without the need of repeated
optimization, or exporting the database or optimized configura-
tion for other purposes. If no output_log is given, no output will
be created.

mode is used to indicate which one should be stored — the database of
fingerprints (argument 1), or generated configuration (argument 2),
or both of them (argument 3), with the first being the default.

***

```
validate val_dir [--state output_state] [--log output_log] [--mode mode]
```

Used for optimization of classifier’s configuration to gain better
results in classifying. This function can be called additionally
to train function if no val_dir was given. If val_dir was used,
this function does not provide any additional functionality. The
meaning of arguments of validate function is the same as in train.

***

```
classify class_dir [--log output_log]
```

Designed for classification of flows captured in pcap files.
class_dir is positional argument containing the path to the di-
rectory with pcap files which are about to be classified. Each
flow in each file will be classified independently as one of the
previously trained protocol or as unknown if no similarity was
found between known protocols and currently classified protocol.

output_log is the path to the file that will be newly created with
the classification results. For each pcap file there will be listed
all flows with the list of 5 most similar trained protocols with
the corresponding score of similarity and the final decision. If no
argument is given, default is standard output.

***

```
test test_dir [--log output_log]
```

Intended for testing the classifier and its current configuration
with standard metrics Accuracy, Precision, Recall, and F1-measure.

test_dir is positional argument referring to the path to directory
containing pcap files with protocols the classifier will be tested
against. The requirement is that this directory has to contain
files named after protocols that were previously trained.

output_log will be newly created text file with test results. Af-
ter classification of each flow from the given directory, overall
statistics of mentioned metrics will be given, so the analyst can
see the overall performance of the classifier, not just metrics for
the individual protocols. If no output_log is specified, standard
output will be used.

***

```
save save_file [--mode mode]
```

Allows to save either the database of protocol fingerprints that
have been trained or the current configuration—selection of fea-
tures. Users can save their trained protocols and later load it
without the need for repeated training. This gives an opportu-
nity to create public databases of known malicious protocols that
can be shared among the users. It is similar to the databases of
rules used by SNORT. Regular users can use SPID4NID only for
classification, whereas security companies and researchers with
the access to malicious traffic could provide such databases along
with the optimized configurations. Data are stored in XML for-
mat which can be easily parsed by any other programs so the
knowledge can be shared without any restrictions.

save_file is positional argument with the name of newly created
XML file.

mode is optional argument used to indicate whether database
of protocol fingerprints (value 1), or configuration (value 2), or
both (value 3) should be stored, with first being the default.
Note that this command can be run additionally to save both of
them.

***

```
load load_file
```

Responsible for loading the XML file with protocol fingerprints
and/or configuration, parsing it and storing into internal struc-
tures of classifier. Functions automatically look for protocol fin-
gerprints and configuration and if found, configures Classifier
accordingly.

help Prints helping message for using the program. Redirects to README file.

## Example
Train the classifier with provided pcap files stored in 'train_dir' directory
and additional pcap files in 'valid_dir' used for optimization and test
the pcap files in 'test_dir' directory:

```
$ ln -s SPID4NID/Debug/SPID4NID ./spid4nid
$ ./spid4nid
>train train_dir --valdir valid_dir --state my_state.xml --log my_output.log --mode 3
>test test_dir
```

Next time, you can reuse the already computed state, so you don't have to wait
for optimization, like this:

```
$ ./spid4nid
>load my_state.xml
>test traffic --log my_test_traffic.log
```

## Acknowledgemts

"The software for this work used the GAlib genetic algorithm package, written by Matthew Wall at the Massachusetts Institute of Technology."

"The software for this work used libedit library."

"The software for this work used libpcap library."

"The software for this work used TinyXML-2 library."

"The software for this work used TinyDir library."

## Contact
maris.adam@gmail.com

This project was part of my diploma thesis available at: https://is.muni.cz/th/g2tob/diploma.pdf

## License
The MIT License (MIT)

Copyright (c) 2016 Adam Maris

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


